/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl    ;

import it.unibas.digitalFileControl.controls.AddDocumentControl;
import it.unibas.digitalFileControl.controls.AddUserDocumentControl;
import it.unibas.digitalFileControl.controls.DocumentDetailControl;
import it.unibas.digitalFileControl.controls.LoginControl;
import it.unibas.digitalFileControl.controls.MenuControl;
import it.unibas.digitalFileControl.controls.NewUserControl;
import it.unibas.digitalFileControl.controls.PrincipalControl;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.LocalStorage;
import it.unibas.digitalFileControl.persistence.DAOAccountService;
import it.unibas.digitalFileControl.persistence.DAODocumentsService;
import it.unibas.digitalFileControl.persistence.DAOUserService;
import it.unibas.digitalFileControl.utils.ConfigurationHelper;
import it.unibas.digitalFileControl.utils.HttpClient;
import it.unibas.digitalFileControl.views.PDFViewerDialog;
import it.unibas.digitalFileControl.views.PDFViewerFrame;
import it.unibas.digitalFileControl.views.PrincipalPanel;
import it.unibas.digitalFileControl.views.ProjectInfoDialog;
import it.unibas.digitalFileControl.views.StartupFrame;
import it.unibas.digitalFileControl.views.ViewFrame;
import javax.swing.SwingUtilities;
import it.unibas.digitalFileControl.persistence.IDAODocuments;
import it.unibas.digitalFileControl.views.AddDocumentDialog;
import it.unibas.digitalFileControl.views.AddUserDocumentDialog;
import it.unibas.digitalFileControl.views.NewUserDialog;
import it.unibas.digitalFileControl.views.DocumentDetailDialog;
import it.unibas.digitalFileControl.views.LoadingDialog;
import it.unibas.digitalFileControl.views.LoginFrame;

/**
 *
 * @author Gabriele D
 */
public class Application {
    private LocalStorage localStorage;
    private IDAODocuments daoDocuments;
    private DAOAccountService daoAccount;
    private ViewFrame frame;
    private MenuControl menuControl;
    private PrincipalPanel principalPanel;
    private PrincipalControl principalControl;
    private PDFViewerDialog pdfViewerDialog;
    private PDFViewerFrame pdfViewerFrame;
    private ProjectInfoDialog projectInfoDialog;
    private StartupFrame startupFrame;
    private LoginFrame loginFrame;
    private LoginControl loginControl;
    private DocumentDetailDialog documentDetailDialog;
    private LoadingDialog loadingDialog;
    private DocumentDetailControl documentDetailControl;
    private AddUserDocumentDialog addUserDocumentDialog;
    private NewUserDialog newUserDialog;
    private AddUserDocumentControl addUserDocumentControl;
    private NewUserControl newUserControl;
    private DAOUserService daoUser;
    private HttpClient httpClient;
    private AddDocumentDialog addDocumentDialog;
    private AddDocumentControl addDocumentControl;
    
    private ConfigurationHelper configurationHelper = new ConfigurationHelper();
    
    private static Application instance = new Application();
    
    public static Application getInstance() {
        return instance;
    }

    public LocalStorage getLocalStorage() {
        return localStorage;
    }

    public AddUserDocumentDialog getAddUserDocumentDialog() {
        return addUserDocumentDialog;
    }

    public NewUserDialog getNewUserDialog() {
        return newUserDialog;
    }

    public AddDocumentControl getAddDocumentControl() {
        return addDocumentControl;
    }

    public AddUserDocumentControl getAddUserDocumentControl() {
        return addUserDocumentControl;
    }

    public IDAODocuments getDaoDocuments() {
        return daoDocuments;
    }

    public DAOUserService getDaoUser() {
        return daoUser;
    }
    
    public DAOAccountService getDaoAccount() {
        return daoAccount;
    }

    public ViewFrame getFrame() {
        return frame;
    }

    public MenuControl getMenuControl() {
        return menuControl;
    }

    public LoadingDialog getLoadingDialog() {
        return loadingDialog;
    }

    public NewUserControl getNewUserControl() {
        return newUserControl;
    }

    public PrincipalPanel getPrincipalPanel() {
        return principalPanel;
    }

    public PrincipalControl getPrincipalControl() {
        return principalControl;
    }

    public AddDocumentDialog getAddDocumentDialog() {
        return addDocumentDialog;
    }
    
    public PDFViewerDialog getPdfViewerDialog() {
        return pdfViewerDialog;
    }
    
    public PDFViewerFrame getPdfViewerFrame() {
        return pdfViewerFrame;
    }
    
    public ProjectInfoDialog getProjectInfoDialog() {
        return projectInfoDialog;
    }
    
    public HttpClient getHttpClient() {
        return httpClient;
    }
    
    public ConfigurationHelper getConfigurationHelper() {
        return configurationHelper;
    }
    
    public StartupFrame getStartupFrame() {
        return startupFrame;
    }

    public DocumentDetailControl getDocumentDetailControl() {
        return documentDetailControl;
    }
    
    public LoginFrame getLoginFrame() {
        return loginFrame;
    }
    
    public LoginControl getLoginControl() {
        return loginControl;
    }
    
    public DocumentDetailDialog getDocumentDetailDialog() {
        return documentDetailDialog;
    }
    
    private Application() {
        
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Application.getInstance().init();
            }
        });
    }
    
    private void init() {      
        Application.getInstance().getConfigurationHelper().loadConfiguration();
        
        // Base View, controls and DAO
        this.localStorage = new LocalStorage();
        this.httpClient = new HttpClient();
        this.daoDocuments = new DAODocumentsService(Application.getInstance().getConfigurationHelper().getValue(Enumerator.Configuration.WEB_SERVICE_URL).toString(), this.httpClient);
        this.daoAccount = new DAOAccountService(Application.getInstance().getConfigurationHelper().getValue(Enumerator.Configuration.WEB_SERVICE_URL).toString(), this.httpClient);
        this.daoUser = new DAOUserService(Application.getInstance().getConfigurationHelper().getValue(Enumerator.Configuration.WEB_SERVICE_URL).toString(), this.httpClient);
        this.frame = new ViewFrame();
        this.menuControl = new MenuControl();
        this.principalPanel = new PrincipalPanel();
        this.principalControl = new PrincipalControl();
        
        // Other view and controls
        this.pdfViewerDialog = new PDFViewerDialog(frame);
        this.pdfViewerFrame = new PDFViewerFrame();
        this.projectInfoDialog = new ProjectInfoDialog(frame);
        this.startupFrame = new StartupFrame();
        this.loginFrame = new LoginFrame();
        this.loginControl = new LoginControl();
        this.documentDetailDialog = new DocumentDetailDialog(frame);
        this.loadingDialog = new LoadingDialog(frame);
        this.addUserDocumentDialog = new AddUserDocumentDialog(frame);
        this.newUserDialog = new NewUserDialog(frame);
        this.documentDetailControl = new DocumentDetailControl();
        this.addUserDocumentControl = new AddUserDocumentControl();
        this.newUserControl = new NewUserControl();
        this.addDocumentDialog = new AddDocumentDialog(frame);
        this.addDocumentControl = new AddDocumentControl();
        
        // Views init
        this.principalPanel.init();
        this.pdfViewerDialog.init();
        this.projectInfoDialog.init();
        this.documentDetailDialog.init();
        this.loadingDialog.init();
        this.addUserDocumentDialog.init();
        this.newUserDialog.init();
        this.addDocumentDialog.init();
        
        // Frames init
        this.frame.init();
        this.pdfViewerFrame.init();
        this.startupFrame.init();
        this.loginFrame.init();
    }
}
