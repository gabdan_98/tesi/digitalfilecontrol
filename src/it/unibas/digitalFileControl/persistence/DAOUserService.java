/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.persistence;

import com.google.gson.JsonObject;
import it.unibas.digitalFileControl.dtos.AuthTokenResponse;
import it.unibas.digitalFileControl.dtos.NewUserResponse;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.HttpResponse;
import it.unibas.digitalFileControl.utils.HttpClient;
import it.unibas.digitalFileControl.utils.JsonHelper;

/**
 *
 * @author Gabriele D
 */
public class DAOUserService {
    
    private String BASE_URL;
    private HttpClient httpClient;
    
    public DAOUserService(String url, HttpClient httpClient) {
        this.BASE_URL = url;
        this.httpClient = httpClient;
    }
    
    public HttpClient getHttpClient() {
        return this.httpClient;
    }
    
    public NewUserResponse saveNewUser(String name, String surname, String email, String username, String password) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/User/AddUser");

        JsonObject request = new JsonObject();
        request.addProperty("name", name);
        request.addProperty("surname", surname);
        request.addProperty("email", email);
        request.addProperty("username", username);
        request.addProperty("password", password);

        HttpResponse httpResponse = httpClient.executePut(url.toString(), request);
        return (NewUserResponse) JsonHelper.parseJson(httpResponse.getResponse(), NewUserResponse.class, httpResponse.getStatusCode());
    }
}