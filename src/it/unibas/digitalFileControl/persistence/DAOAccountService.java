/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.persistence;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import it.unibas.digitalFileControl.dtos.AuthTokenResponse;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.HttpResponse;
import it.unibas.digitalFileControl.utils.HttpClient;
import it.unibas.digitalFileControl.utils.JsonHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Gabriele D
 */
public class DAOAccountService {
    
    private String BASE_URL;
    private HttpClient httpClient;
    
    public DAOAccountService(String url, HttpClient httpClient) {
        this.BASE_URL = url;
        this.httpClient = httpClient;
    }
    
    public HttpClient getHttpClient() {
        return this.httpClient;
    }
    
    public AuthTokenResponse getAuthTokens(String username, String password) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/Account/authenticate");

        JsonObject request = new JsonObject();
        request.addProperty("username", username);
        request.addProperty("password", password);

        HttpResponse httpResponse = httpClient.executePost(url.toString(), request);
        return (AuthTokenResponse) JsonHelper.parseJson(httpResponse.getResponse(), AuthTokenResponse.class, httpResponse.getStatusCode());
    }
    
    public AuthTokenResponse refreshTokens(String refreshToken) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/Account/refreshToken");

        JsonObject request = new JsonObject();
        request.addProperty("refreshToken", refreshToken);

        HttpResponse httpResponse = httpClient.executePost(url.toString(), request);
        return (AuthTokenResponse) JsonHelper.parseJson(httpResponse.getResponse(), AuthTokenResponse.class, httpResponse.getStatusCode());
    }
}