/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.persistence;

/**
 *
 * @author Gabriele D
 */
public class CustomDAOServiceResponse {
    private int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
    
    public boolean isSuccess() {
        return this.getStatusCode() == 200;
    }
    
    public boolean isPreconditionFailed() {
        return this.getStatusCode() == 412;
    }
    
    public boolean isForbidden() {
        return this.getStatusCode() == 403;
    }
    
    public boolean isNotFound() {
        return this.getStatusCode() == 404;
    }
    
    public boolean isLocked() {
        return this.getStatusCode() == 423;
    }
    
    public boolean isBadRequest() {
        return this.getStatusCode() == 400;
    }
    
    public boolean isTooManyRequests() {
        return this.getStatusCode() == 429;
    }
}