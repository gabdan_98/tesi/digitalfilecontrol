/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.persistence;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import it.unibas.digitalFileControl.dtos.AddDocumentRequest;
import it.unibas.digitalFileControl.dtos.AddDocumentResponse;
import it.unibas.digitalFileControl.dtos.AddUserDocumentRequest;
import it.unibas.digitalFileControl.dtos.DeleteDocumentResponse;
import it.unibas.digitalFileControl.dtos.DeleteUserFromDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetAddUserDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentDetailResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetMySharedDocumentsResponse;
import it.unibas.digitalFileControl.dtos.GetSharedWithMeDocumentsResponse;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.model.HttpResponse;
import it.unibas.digitalFileControl.utils.FileHelper;
import it.unibas.digitalFileControl.utils.HttpClient;
import it.unibas.digitalFileControl.utils.JsonHelper;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gabriele D
 */
public class DAODocumentsService implements IDAODocuments {
    
    private String BASE_URL;
    private HttpClient httpClient;
    
    public DAODocumentsService(String url, HttpClient httpClient) {
        this.BASE_URL = url;
        this.httpClient = httpClient;
    }
    
    public HttpClient getHttpClient() {
        return this.httpClient;
    }

    @Override
    public GetSharedWithMeDocumentsResponse getSharedWithMeDocuments() throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/Document/GetSharedWithMeDocuments");
        
        HttpResponse httpResponse = httpClient.executeGet(url.toString());
        return (GetSharedWithMeDocumentsResponse)JsonHelper.parseJson(httpResponse.getResponse(), GetSharedWithMeDocumentsResponse.class, httpResponse.getStatusCode());
    }
    
    @Override
    public GetMySharedDocumentsResponse getMySharedDocuments() throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/Document/GetMySharedDocuments");
        
        HttpResponse httpResponse = httpClient.executeGet(url.toString());
        return (GetMySharedDocumentsResponse)JsonHelper.parseJson(httpResponse.getResponse(), GetMySharedDocumentsResponse.class, httpResponse.getStatusCode());
    }

    @Override
    public GetDocumentResponse getDocument(int id, List<Document> userDocuments) throws Exception {
        return getDocumentById(id, userDocuments);
    }
    
    private GetDocumentResponse getDocumentById(int id, List<Document> userDocuments) throws Exception {
        for(Document f : userDocuments) {
            if(f.getId() == id) {
                StringBuilder url = new StringBuilder();
                url.append(BASE_URL);
                url.append("/Document/GetDocument/");
                url.append(f.getId());
                
                HttpResponse httpResponse = httpClient.executeGet(url.toString());
                return (GetDocumentResponse) JsonHelper.parseJson(httpResponse.getResponse(), GetDocumentResponse.class, httpResponse.getStatusCode());
            }
        }
        return null;
    }

    @Override
    public GetDocumentDetailResponse getDocumentDetails(int id) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/Document/GetDocumentDetails/");
        url.append(id);
        
        HttpResponse httpResponse = httpClient.executeGet(url.toString());
        return (GetDocumentDetailResponse) JsonHelper.parseJson(httpResponse.getResponse(), GetDocumentDetailResponse.class, httpResponse.getStatusCode());
    }

    @Override
    public DeleteUserFromDocumentResponse revokeUserDocument(int id) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/UserDocument/DeleteUserFromDocument/");
        url.append(id);
        
        HttpResponse httpResponse = httpClient.executeDelete(url.toString(), null);
        return (DeleteUserFromDocumentResponse) JsonHelper.parseJson(httpResponse.getResponse(), DeleteUserFromDocumentResponse.class, httpResponse.getStatusCode());
    }

    @Override
    public GetAddUserDocumentResponse addUserDocument(AddUserDocumentRequest requestModel) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/UserDocument/AddUserToDocument");
        
        JsonObject request = new JsonObject();
        request.addProperty("UserId", requestModel.getUserId());
        request.addProperty("DocumentId", requestModel.getDocumentId());
        request.addProperty("CanView", requestModel.isCanView());
        request.addProperty("CanPrint", requestModel.isCanPrint());
        request.addProperty("CanDownload", requestModel.isCanDownload());
        request.addProperty("MaxViewsNumber", requestModel.getMaxViewsNumber());
        
        HttpResponse httpResponse = httpClient.executePut(url.toString(), request);
        return (GetAddUserDocumentResponse) JsonHelper.parseJson(httpResponse.getResponse(), GetAddUserDocumentResponse.class, httpResponse.getStatusCode());
    }

    @Override
    public AddDocumentResponse addNewDocument(AddDocumentRequest requestModel) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/Document/AddDocument");
        
        JsonObject request = new JsonObject();
        try {
            request.addProperty("Name", requestModel.getName());
            request.addProperty("Description", requestModel.getDescription());
            request.addProperty("DocumentExpire", requestModel.isDocumentExpire());
            request.addProperty("ExpirationTime", requestModel.getExpirationTime());
            request.addProperty("DocumentTypeId", requestModel.getDocumentTypeId());
            request.addProperty("Content", requestModel.getContent());
        } catch (Exception ex) {
            
        }
        
        HttpResponse httpResponse = httpClient.executePut(url.toString(), request);
        return (AddDocumentResponse) JsonHelper.parseJson(httpResponse.getResponse(), AddDocumentResponse.class, httpResponse.getStatusCode());
    }

    @Override
    public DeleteDocumentResponse deleteDocument(int id) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(BASE_URL);
        url.append("/Document/DeleteDocument/");
        url.append(id);
        
        HttpResponse httpResponse = httpClient.executeDelete(url.toString(), null);
        return (DeleteDocumentResponse) JsonHelper.parseJson(httpResponse.getResponse(), DeleteDocumentResponse.class, httpResponse.getStatusCode());
    }
}