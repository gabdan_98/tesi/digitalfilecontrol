/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package it.unibas.digitalFileControl.persistence;

import it.unibas.digitalFileControl.dtos.AddDocumentRequest;
import it.unibas.digitalFileControl.dtos.AddDocumentResponse;
import it.unibas.digitalFileControl.dtos.AddUserDocumentRequest;
import it.unibas.digitalFileControl.dtos.DeleteDocumentResponse;
import it.unibas.digitalFileControl.dtos.DeleteUserFromDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetAddUserDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentDetailResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetMySharedDocumentsResponse;
import it.unibas.digitalFileControl.dtos.GetSharedWithMeDocumentsResponse;
import it.unibas.digitalFileControl.model.Document;
import java.util.List;

/**
 *
 * @author Gabriele D
 */
public interface IDAODocuments {
    
    public GetSharedWithMeDocumentsResponse getSharedWithMeDocuments() throws Exception;
    public GetMySharedDocumentsResponse getMySharedDocuments() throws Exception;
    public GetDocumentResponse getDocument(int id, List<Document> files) throws Exception;
    public GetDocumentDetailResponse getDocumentDetails(int id) throws Exception;
    public DeleteUserFromDocumentResponse revokeUserDocument(int id) throws Exception;
    public GetAddUserDocumentResponse addUserDocument(AddUserDocumentRequest request) throws Exception;
    public AddDocumentResponse addNewDocument(AddDocumentRequest request) throws Exception;
    public DeleteDocumentResponse deleteDocument(int id) throws Exception;
}