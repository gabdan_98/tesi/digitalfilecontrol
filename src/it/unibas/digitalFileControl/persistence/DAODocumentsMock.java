/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.persistence;

import it.unibas.digitalFileControl.dtos.AddDocumentRequest;
import it.unibas.digitalFileControl.dtos.AddDocumentResponse;
import it.unibas.digitalFileControl.dtos.AddUserDocumentRequest;
import it.unibas.digitalFileControl.dtos.DeleteDocumentResponse;
import it.unibas.digitalFileControl.dtos.DeleteUserFromDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetAddUserDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentDetailResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetMySharedDocumentsResponse;
import it.unibas.digitalFileControl.dtos.GetSharedWithMeDocumentsResponse;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.utils.FileHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Gabriele D
 */
public class DAODocumentsMock implements IDAODocuments {

    @Override
    public GetSharedWithMeDocumentsResponse getSharedWithMeDocuments() {
        List<Document> list = new ArrayList<Document>();
        String content = "";
        
        list.add(new Document(1, 1, "Delega", "Documento generico di delega", Enumerator.FileTypes.PDF, true, true, true, new GregorianCalendar(2022, Calendar.DECEMBER, 31).getTime(), new GregorianCalendar(2022, Calendar.DECEMBER, 31).getTime(), content, "delega.txt"));
        list.add(new Document(2, 2, "Privacy", "Modello protezione dati personali", Enumerator.FileTypes.PDF, true, true, true, new GregorianCalendar(2022, Calendar.OCTOBER, 14).getTime(), new GregorianCalendar(2022, Calendar.DECEMBER, 31).getTime(), content, "privacy.txt"));
        list.add(new Document(3, 3, "Cud", "Modello certificazione unica", Enumerator.FileTypes.PDF, true, true, true, new GregorianCalendar(2023, Calendar.FEBRUARY, 02).getTime(), new GregorianCalendar(2022, Calendar.DECEMBER, 31).getTime(), content, "cud.txt"));
        
        return new GetSharedWithMeDocumentsResponse(list);
    }
    
    @Override
    public GetMySharedDocumentsResponse getMySharedDocuments() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public GetDocumentResponse getDocument(int id, List<Document> files) {
        return new GetDocumentResponse(getDocumentById(id, files));
    }
    
    private Document getDocumentById(int id, List<Document> files) {
        Document file = new Document();
        for(Document f : files) {
            if(f.getId() == id) {
                file = f;
                StringBuilder path = new StringBuilder();
                path.append("mock/");
                path.append(file.getPhisicalName());
                String content = FileHelper.readTextFromFile(path.toString());
                file.setContent(content);
            }
        }
        return file;
    }

    @Override
    public GetDocumentDetailResponse getDocumentDetails(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public DeleteUserFromDocumentResponse revokeUserDocument(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public GetAddUserDocumentResponse addUserDocument(AddUserDocumentRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public AddDocumentResponse addNewDocument(AddDocumentRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public DeleteDocumentResponse deleteDocument(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}