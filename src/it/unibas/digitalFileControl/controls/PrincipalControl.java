/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.controls;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.dtos.DeleteDocumentResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentDetailResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentResponse;
import it.unibas.digitalFileControl.exception.EncryptionException;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.model.MySharedTableModel;
import it.unibas.digitalFileControl.model.SharedWithMeTableModel;
import it.unibas.digitalFileControl.persistence.DAODocumentsMock;
import it.unibas.digitalFileControl.utils.EncryptionHelper;
import it.unibas.digitalFileControl.utils.ViewerHelper;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ProcessBuilder.Redirect.Type;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTable;

/**
 *
 * @author Gabriele D
 */
public class PrincipalControl {
    private Action addDocumentAction = new AddDocumentAction();
    private Action deleteDocumentAction = new DeleteDocumentAction();

    public Action getAddDocumentAction() {
        return addDocumentAction;
    }

    public Action getDeleteDocumentAction() {
        return deleteDocumentAction;
    }
    
    private class DeleteDocumentAction extends AbstractAction {
        
        public DeleteDocumentAction() {
           putValue(Action.NAME, "Elimina documento");
           putValue(Action.SHORT_DESCRIPTION, "Elimina il documento selezionato");
       }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            int selectedIndex = Application.getInstance().getPrincipalPanel().getMySharedSelectedRow();
            if(selectedIndex == -1) {
                Application.getInstance().getFrame().showInfoMessage("Seleziona prima un documento dalla tabella");
                return;
            }
            List<Document> mySharedDocuments = (List<Document>) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.MY_SHARED_DOCUMENTS);
            Document documentToDelete = mySharedDocuments.get(selectedIndex);
            try {
                Application.getInstance().getLoadingDialog().showDialog();
                new Thread(() -> {
                    try {
                        DeleteDocumentResponse httpResponse = Application.getInstance().getDaoDocuments().deleteDocument(documentToDelete.getId());
                        Application.getInstance().getLoadingDialog().hideDialog();
                        if(httpResponse.isSuccess()) {
                            mySharedDocuments.remove(selectedIndex);
                            Application.getInstance().getPrincipalPanel().fillMySharedTable();
                        } else if(httpResponse.isForbidden()) {
                            Application.getInstance().getFrame().showErrorMessage("Non sei autorizzato ad effettuare l'operazione");
                        } else {
                            Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                        }
                    } catch (Exception ex) {
                        Application.getInstance().getLoadingDialog().hideDialog();
                        Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                    }
                }).start();
            } catch (Exception ex) {
                Application.getInstance().getLoadingDialog().hideDialog();
                Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
            }
        }
        
    }
    
    private class AddDocumentAction extends AbstractAction {
        
       public AddDocumentAction() {
           putValue(Action.NAME, "Proteggi un nuovo documento");
           putValue(Action.SHORT_DESCRIPTION, "Proteggi un nuovo documento");
       }

        @Override
        public void actionPerformed(ActionEvent e) {
            Application.getInstance().getAddDocumentDialog().showDialog();
        }
    }
    
    public void setSharedWithMeTableOpenDocumentAction() {
        Application.getInstance().getPrincipalPanel().getSharedWithMeTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked (MouseEvent me) {
                if(me.getClickCount() == 2) {
                    doOpenDocumentViewAction();
                }
            }
        });
    }
    
    public void setMySharedTableOpenDocumentAction() {
        Application.getInstance().getPrincipalPanel().getMySharedTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked (MouseEvent me) {
                if(me.getClickCount() == 2) {
                    doOpenDocumentDetailAction();
                }
            }
        });
    }
    
    private void doOpenDocumentViewAction() {
        int selectedIndex = Application.getInstance().getPrincipalPanel().getSharedWithMeTable().getSelectedRow();
        List<Document> documents = (List<Document>)Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.SHARED_WITH_ME_DOCUMENTS);
        int selectedDocumentId = documents.get(selectedIndex).getId();
        try {
            Application.getInstance().getLoadingDialog().showDialog();
            new Thread(() -> {
                try {
                    List<Document> userDocuments = (List<Document>)Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.SHARED_WITH_ME_DOCUMENTS);
                    GetDocumentResponse httpResponse = Application.getInstance().getDaoDocuments().getDocument(selectedDocumentId, userDocuments);
                    Application.getInstance().getLoadingDialog().hideDialog();
                    if(httpResponse.isForbidden()) {
                        Application.getInstance().getFrame().showInfoMessage("Non sei autorizzato a visualizzare questo documento");
                        return;
                    }
                    if(httpResponse.isPreconditionFailed()) {
                        Application.getInstance().getFrame().showInfoMessage("Hai superato il numero di visualizzazioni a tua disposizione");
                        return;
                    }
                    if(httpResponse.isLocked()) {
                        Application.getInstance().getFrame().showInfoMessage("Il documento è scaduto");
                        return;
                    }
                    Document selectedDocument = httpResponse.getDocument();
                    if(selectedDocument == null) {
                        Application.getInstance().getFrame().showErrorMessage("Non è stato possibile aprire il documento oppure non disponi dell'autorizzazione");
                        return;
                    }
                    Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.VIEW_SELECTED_DOCUMENT, selectedDocument);
                    String fileContent = "";
                    try
                    {
                        fileContent = EncryptionHelper.decrypt(selectedDocument.getContent());
                    }
                    catch (EncryptionException ex) {
                        System.out.println("Encryption exception");
                        Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                        return;
                    }
                    ViewerHelper.openPDFDocument(fileContent, selectedDocument.isCanView(), selectedDocument.isCanPrint(), selectedDocument.isCanDownload());
                } catch (Exception ex) {
                    Application.getInstance().getLoadingDialog().hideDialog();
                    Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                }
            }).start();
        } catch (Exception ex) {
            Application.getInstance().getLoadingDialog().hideDialog();
            Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
        }
    }
    
    private void doOpenDocumentDetailAction() {
        int selectedIndex = Application.getInstance().getPrincipalPanel().getMySharedTable().getSelectedRow();
        MySharedTableModel tableModel = (MySharedTableModel)Application.getInstance().getPrincipalPanel().getMySharedTable().getModel();
        int selectedDocumentId = tableModel.getDocumentByIndex(selectedIndex).getId();
        try {
            Application.getInstance().getLoadingDialog().showDialog();
            new Thread(() -> {
                try {
                    GetDocumentDetailResponse response = Application.getInstance().getDaoDocuments().getDocumentDetails(selectedDocumentId);
                    Application.getInstance().getLoadingDialog().hideDialog();
                    Application.getInstance().getDocumentDetailDialog().setDocumentDetails(response.getDocument());
                    Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.DETAIL_SELECTED_DOCUMENT, response.getDocument());
                    Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.DETAILS_USERDOCUMENTS, response.getUserDocuments());
                    Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.DETAILS_UNAUTHORIZED_USERS, response.getUnauthorizedUsers());
                    Application.getInstance().getDocumentDetailDialog().loadTablesData();
                    Application.getInstance().getDocumentDetailDialog().showDialog();
                } catch (Exception ex) {
                    Application.getInstance().getLoadingDialog().hideDialog();
                    Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                }
            }).start();
        } catch (Exception ex) {
            Application.getInstance().getLoadingDialog().hideDialog();
            Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
        }
    }
}