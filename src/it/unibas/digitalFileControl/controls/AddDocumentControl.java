/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.controls;

import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.dtos.AddDocumentRequest;
import it.unibas.digitalFileControl.dtos.AddDocumentResponse;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.model.Enumerator;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileFilter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;

/**
 *
 * @author Gabriele D
 */
public class AddDocumentControl {
    private Action level1SelectionAction = new Level1SelectionAction();
    private Action level2SelectionAction = new Level2SelectionAction();
    private Action level3SelectionAction = new Level3SelectionAction();
    private Action uploadFileAction = new UploadFileAction();
    private Action protectDocumentAction = new ProtectDocumenteAction();
    private Action enableDocumentExpirationAction = new EnableDocumentExpirationAction();

    public Action getLevel1SelectionAction() {
        return level1SelectionAction;
    }

    public Action getProtectDocumentAction() {
        return protectDocumentAction;
    }

    public Action getEnableDocumentExpirationAction() {
        return enableDocumentExpirationAction;
    }

    public Action getUploadFileAction() {
        return uploadFileAction;
    }

    public Action getLevel2SelectionAction() {
        return level2SelectionAction;
    }

    public Action getLevel3SelectionAction() {
        return level3SelectionAction;
    }
    
    private class EnableDocumentExpirationAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            Application.getInstance().getAddDocumentDialog().changeExpirationDateSpinnerState();
        }
    }
    
    private class ProtectDocumenteAction extends AbstractAction {
        public ProtectDocumenteAction() {
            putValue(Action.NAME, "Proteggi documento");
            putValue(Action.SHORT_DESCRIPTION, "Proteggi documento");
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if(Application.getInstance().getAddDocumentDialog().isLevel1Selected() || Application.getInstance().getAddDocumentDialog().isLevel2Selected()) {
                try {
                    Object uploadedFile = Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.UPLOADED_FILE_PATH);
                    if(uploadedFile == null) {
                        Application.getInstance().getFrame().showInfoMessage("Carica prima un file");
                        return;
                    }
                    PDDocument doc = PDDocument.load((File)uploadedFile);
                    AccessPermission ap = new AccessPermission();
                    boolean allowPrint = false;
                    boolean allowCopy = false;
                    String documentPassword = "";
                    
                    if(Application.getInstance().getAddDocumentDialog().isLevel1Selected()) {
                        allowPrint = Application.getInstance().getAddDocumentDialog().isLevel1AllowPrint();
                        allowCopy = Application.getInstance().getAddDocumentDialog().isLevel1AllowCopy();
                    } else {
                        allowPrint = Application.getInstance().getAddDocumentDialog().isLevel2AllowPrint();
                        allowCopy = Application.getInstance().getAddDocumentDialog().isLevel2AllowCopy();
                        String password = Application.getInstance().getAddDocumentDialog().getDocumentPassword();
                        if(password.equalsIgnoreCase("")) {
                            Application.getInstance().getFrame().showInfoMessage("Inserisci una password per il documento");
                            return;
                        }
                        documentPassword = password;
                    }
                    
                    ap.setCanPrint(allowPrint);
                    ap.setCanExtractContent(allowCopy);
                    ap.setCanModify(false);
                    ap.setCanModifyAnnotations(false);
                    StandardProtectionPolicy spp = new StandardProtectionPolicy("PG-2tdg*d?dgm_SF^@d#sfdd12|?=!58", documentPassword, ap);
                    doc.protect(spp);
                    JFileChooser fileChooser = new JFileChooser();
                    int option = fileChooser.showSaveDialog(Application.getInstance().getAddDocumentDialog());
                    if(option == JFileChooser.APPROVE_OPTION){
                        File file = fileChooser.getSelectedFile();
                        Application.getInstance().getFrame().showInfoMessage("File salvato correttamente");
                        String pathToSave = file.getAbsolutePath();
                        if(!pathToSave .endsWith(".pdf")) {
                            pathToSave += ".pdf";
                        }
                        doc.save(pathToSave);
                        doc.close();
                     }
                }
                catch (Exception ex) {
                    Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                }
            } else {
                Object uploadedFile = Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.UPLOADED_FILE_PATH);
                if(uploadedFile == null) {
                    Application.getInstance().getFrame().showInfoMessage("Carica prima un file");
                    return;
                }
                String documentName = Application.getInstance().getAddDocumentDialog().getDocumentName();
                String documentDescription = Application.getInstance().getAddDocumentDialog().getDocumentDescription();
                boolean isDocumentExpire = Application.getInstance().getAddDocumentDialog().isDocumentExpire();
                Date expirationTime = Application.getInstance().getAddDocumentDialog().getDocumentExpirationDate();
                
                if(documentName.equalsIgnoreCase("")) {
                    Application.getInstance().getFrame().showInfoMessage("Inserisci il nome del documento");
                    return;
                }
                
                if(documentDescription.equalsIgnoreCase("")) {
                    Application.getInstance().getFrame().showInfoMessage("Inserisci la descrizione del documento");
                    return;
                }
                try {
                    File file = (File) uploadedFile;
                    String base64String = Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath()));
                    Application.getInstance().getLoadingDialog().showDialog();
                    Long expirationTimeValue = null;
                    Date effectiveExpirationTime;
                    if(isDocumentExpire) {
                        expirationTimeValue = expirationTime.getTime();
                        effectiveExpirationTime = expirationTime;
                    } else {
                        effectiveExpirationTime = null;
                    }
                    AddDocumentRequest request = new AddDocumentRequest(documentName, documentDescription, isDocumentExpire, expirationTimeValue, Enumerator.FileTypes.PDF, base64String);
                    new Thread(() -> {
                        try {
                            AddDocumentResponse response = Application.getInstance().getDaoDocuments().addNewDocument(request);
                            Application.getInstance().getLoadingDialog().hideDialog();
                            if(response.isSuccess()) {
                                List<Document> mySharedDocuments = (List<Document>) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.MY_SHARED_DOCUMENTS);
                                mySharedDocuments.add(new Document(0, response.getId(), documentName, documentDescription, Enumerator.FileTypes.PDF, true, true, true, new Date(), effectiveExpirationTime, base64String, ""));
                                Application.getInstance().getPrincipalPanel().fillMySharedTable();
                                Application.getInstance().getAddDocumentDialog().hideDialog();
                            }
                        }
                        catch (Exception ex) {
                            Application.getInstance().getLoadingDialog().hideDialog();
                            Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                        }
                    }).start();
                     
                } catch (Exception ex) {
                    Application.getInstance().getLoadingDialog().hideDialog();
                    Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                }
            }
        }
    }
    
    private class UploadFileAction extends AbstractAction {
        public UploadFileAction() {
            putValue(Action.NAME, "Carica file");
            putValue(Action.SHORT_DESCRIPTION, "Carica file");
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("File PDF", "pdf");
            fileChooser.addChoosableFileFilter(filter);
            
            int result = fileChooser.showOpenDialog(Application.getInstance().getAddDocumentDialog());
            if (result == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.UPLOADED_FILE_PATH, selectedFile.getAbsoluteFile());
            }
        }
        
    }
    
    private class Level1SelectionAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(Application.getInstance().getAddDocumentDialog().isLevel1Selected()) {
                Application.getInstance().getAddDocumentDialog().setLevel1Selection();
            } else {
                Application.getInstance().getAddDocumentDialog().setLevel1Deselection();
            }
        }
    }
    
    private class Level2SelectionAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(Application.getInstance().getAddDocumentDialog().isLevel2Selected()) {
                Application.getInstance().getAddDocumentDialog().setLevel2Selection();
            } else {
                Application.getInstance().getAddDocumentDialog().setLevel2Deselection();
            }
        }
    }
    
    private class Level3SelectionAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(Application.getInstance().getAddDocumentDialog().isLevel3Selected()) {
                Application.getInstance().getAddDocumentDialog().setLevel3Selection();
            } else {
                Application.getInstance().getAddDocumentDialog().setLevel3Deselection();
            }
        }
    }
}
