/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.controls;

import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.dtos.NewUserResponse;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.User;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 *
 * @author Gabriele D
 */
public class NewUserControl {
    private Action saveNewUserAction = new SaveNewUserAction();
    
    public Action getSaveNewUserAction() {
        return saveNewUserAction;
    }
    
    private class SaveNewUserAction extends AbstractAction {
        public SaveNewUserAction() {
            putValue(Action.NAME, "Salva");
            putValue(Action.SHORT_DESCRIPTION, "Salva l'utente");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String name = Application.getInstance().getNewUserDialog().getName();
            String surname = Application.getInstance().getNewUserDialog().getSurname();
            String email = Application.getInstance().getNewUserDialog().getEmail();
            String username = Application.getInstance().getNewUserDialog().getUsername();
            String password = Application.getInstance().getNewUserDialog().getPassword();
            
            if(name.equalsIgnoreCase("") || surname.equalsIgnoreCase("") || email.equalsIgnoreCase("") || username.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
                Application.getInstance().getFrame().showInfoMessage("Compila tutti i campi");
                return;
            }
            
            try {
                Application.getInstance().getLoadingDialog().showDialog();
                new Thread(() -> {
                    try {
                        NewUserResponse response = Application.getInstance().getDaoUser().saveNewUser(name, surname, email, username, password);
                        Application.getInstance().getLoadingDialog().hideDialog();
                        if(response.isSuccess()) {
                            List<User> usersList = (List<User>) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.DETAILS_UNAUTHORIZED_USERS);
                            usersList.add(new User(response.getId(), name, surname, username, email));
                            Application.getInstance().getDocumentDetailDialog().loadTablesData();
                            Application.getInstance().getNewUserDialog().hideDialog();
                            return;
                        } else if(response.isBadRequest()) {
                            Application.getInstance().getFrame().showInfoMessage("Username o email già utilizzati");
                            return;
                        } else {
                            Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                        }
                    }
                    catch (Exception ex) {
                        Application.getInstance().getLoadingDialog().hideDialog();
                        Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                    }
                }).start();
            } catch (Exception ex) {
                Application.getInstance().getLoadingDialog().hideDialog();
                Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
            }
        }
    }
}
