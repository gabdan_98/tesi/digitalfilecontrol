/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.controls;

import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.dtos.AddUserDocumentRequest;
import it.unibas.digitalFileControl.dtos.DeleteUserFromDocumentResponse;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.User;
import it.unibas.digitalFileControl.model.UserDocument;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

/**
 *
 * @author Gabriele D
 */
public class DocumentDetailControl {
    private Action openNewUserDialogAction = new OpenNewUserDialogAction();

    public Action getOpenNewUserDialogAction() {
        return openNewUserDialogAction;
    }
    
    public void setAuthorizedTableRemoveUserAction() {
        Application.getInstance().getDocumentDetailDialog().getAuthorizedUsersTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked (MouseEvent me) {
                if(me.getClickCount() == 2) {
                    doRemoveUserAction();
                }
            }
        });
    }
    
    public void setUnhautorizedTableAddUserAction() {
        Application.getInstance().getDocumentDetailDialog().getUnauthorizedUsersTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked (MouseEvent me) {
                if(me.getClickCount() == 2) {
                    doAddUserToDocumentAction();
                }
            }
        });
    }
    
    private void doRemoveUserAction() {
        new Thread(() -> {
            try {
                int selectedUserDocumentIndex = Application.getInstance().getDocumentDetailDialog().getSelectedUserDocument();
                if(selectedUserDocumentIndex == -1) {
                    Application.getInstance().getFrame().showInfoMessage("Seleziona un utente a cui revocare l'autorizzazione");
                    return;
                }
                List<UserDocument> userDocumentList = (List<UserDocument>)Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.DETAILS_USERDOCUMENTS);
                UserDocument selectedUserDocument = userDocumentList.get(selectedUserDocumentIndex);
                Application.getInstance().getLoadingDialog().showDialog();
                DeleteUserFromDocumentResponse response = Application.getInstance().getDaoDocuments().revokeUserDocument(selectedUserDocument.getId());
                Application.getInstance().getLoadingDialog().hideDialog();
                if(response.isNotFound()) {
                    Application.getInstance().getFrame().showInfoMessage("Documento non trovato");
                }
                if(response.isForbidden()) {
                    Application.getInstance().getFrame().showInfoMessage("Non hai i permessi per effettuare l'operazione");
                }
                if(response.isSuccess()) {
                    userDocumentList.remove(selectedUserDocumentIndex);
                    List<User> unauthorizedUsersList = (List<User>) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.DETAILS_UNAUTHORIZED_USERS);
                    User userToAdd = new User();
                    userToAdd.setId(selectedUserDocument.getUserId());
                    userToAdd.setName(selectedUserDocument.getName());
                    userToAdd.setSurname(selectedUserDocument.getSurname());
                    userToAdd.setEmail(selectedUserDocument.getEmail());
                    userToAdd.setUsername(selectedUserDocument.getUsername());
                    unauthorizedUsersList.add(userToAdd);
                    Application.getInstance().getDocumentDetailDialog().loadTablesData();
                }
            } catch (Exception ex) {
                Application.getInstance().getLoadingDialog().hideDialog();
                Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
            }
        }).start();
    }
    
    private void doAddUserToDocumentAction() {
        int selectedUserIndex = Application.getInstance().getDocumentDetailDialog().getSelectedUnauthorizedUser();
        if(selectedUserIndex == -1) {
            Application.getInstance().getFrame().showInfoMessage("Seleziona un utente a cui concedere l'autorizzazione");
            return;
        }
        Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.ADD_USERDOCUMENT_SELECTED_INDEX, selectedUserIndex);
        List<User> unauthorizedUsersList = (List<User>)Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.DETAILS_UNAUTHORIZED_USERS);
        User selectedUser = unauthorizedUsersList.get(selectedUserIndex);
        Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.ADD_USERDOCUMENT_USER, selectedUser);
        AddUserDocumentRequest addUserRequest = new AddUserDocumentRequest();
        addUserRequest.setUserId(selectedUser.getId());
        Document selectedDocument = (Document) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.DETAIL_SELECTED_DOCUMENT);
        addUserRequest.setDocumentId(selectedDocument.getId());
        Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.ADD_USERDOCUMENT_REQUEST, addUserRequest);
        Application.getInstance().getAddUserDocumentDialog().showDialog();
    }
    
    private class OpenNewUserDialogAction extends AbstractAction {
        
        public OpenNewUserDialogAction() {
            putValue(Action.NAME, "Nuovo utente");
            putValue(Action.SHORT_DESCRIPTION, "Apre la finestra di creazione utente");
            putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("Ctrl N"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            Application.getInstance().getNewUserDialog().showDialog();
        }
    }
}
