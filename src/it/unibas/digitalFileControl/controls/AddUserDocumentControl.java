/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.controls;

import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.dtos.AddUserDocumentRequest;
import it.unibas.digitalFileControl.dtos.GetAddUserDocumentResponse;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.User;
import it.unibas.digitalFileControl.model.UserDocument;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

/**
 *
 * @author Gabriele D
 */
public class AddUserDocumentControl {
    private Action saveUserDocumentAction = new SaveUserDocumentAction();
    private Action changeSpinnerStateAction = new ChangeSpinnerStateAction();
    private Action checkboxSelectionAction = new CheckboxSelectionAction();
    private Action viewCheckboxSelectionAction = new ViewCheckboxSelectionAction();

    public Action getSaveUserDocumentAction() {
        return saveUserDocumentAction;
    }

    public Action getViewCheckboxSelectionAction() {
        return viewCheckboxSelectionAction;
    }

    public Action getCheckboxSelectionAction() {
        return checkboxSelectionAction;
    }

    public Action getChangeSpinnerStateAction() {
        return changeSpinnerStateAction;
    }
    
    private class ViewCheckboxSelectionAction extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            if(!Application.getInstance().getAddUserDocumentDialog().isCanView()) {
                Application.getInstance().getAddUserDocumentDialog().setCanPrintCheckboxUnselected();
                Application.getInstance().getAddUserDocumentDialog().setCanDownloadCheckboxUnselected();
                Application.getInstance().getAddUserDocumentDialog().setViewsLimitCheckboxUnselected();
            }
        }
    }
    
    private class CheckboxSelectionAction extends AbstractAction {
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if(Application.getInstance().getAddUserDocumentDialog().isCanPrint() || Application.getInstance().getAddUserDocumentDialog().isCanDownload() || Application.getInstance().getAddUserDocumentDialog().isViewsLimit()) {
                Application.getInstance().getAddUserDocumentDialog().setCanViewCheckboxSelected();
            }
        }
    }

    private class ChangeSpinnerStateAction extends AbstractAction {
        
        public ChangeSpinnerStateAction() {
            putValue(Action.NAME, "Limite di visualizzazioni");
            putValue(Action.SHORT_DESCRIPTION, "Limite di visualizzazioni");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Application.getInstance().getAddUserDocumentDialog().changeMaxViewsSpinnerState();
            if(Application.getInstance().getAddUserDocumentDialog().isCanPrint() || Application.getInstance().getAddUserDocumentDialog().isCanDownload() || Application.getInstance().getAddUserDocumentDialog().isViewsLimit()) {
                Application.getInstance().getAddUserDocumentDialog().setCanViewCheckboxSelected();
            }
        }
    }
    
    private class SaveUserDocumentAction extends AbstractAction {
        
        public SaveUserDocumentAction() {
            putValue(Action.NAME, "Salva");
            putValue(Action.SHORT_DESCRIPTION, "Salva");
            putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("Ctrl S"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isCanView = Application.getInstance().getAddUserDocumentDialog().isCanView();
            boolean isCanPrint = Application.getInstance().getAddUserDocumentDialog().isCanPrint();
            boolean isCanDownload = Application.getInstance().getAddUserDocumentDialog().isCanDownload();
            boolean isViewsLimit = Application.getInstance().getAddUserDocumentDialog().isViewsLimit();
            Integer maxViewsNumber = isViewsLimit == false ? null : Application.getInstance().getAddUserDocumentDialog().getMaxViewsNumber();
            
            AddUserDocumentRequest request = (AddUserDocumentRequest) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.ADD_USERDOCUMENT_REQUEST);
            request.setCanView(isCanView);
            request.setCanPrint(isCanPrint);
            request.setCanDownload(isCanDownload);
            request.setMaxViewsNumber(maxViewsNumber);
            
            try {
                Application.getInstance().getLoadingDialog().showDialog();
                new Thread(() -> {
                    try {
                        GetAddUserDocumentResponse response = Application.getInstance().getDaoDocuments().addUserDocument(request);
                        Application.getInstance().getLoadingDialog().hideDialog();
                        if(response.isSuccess()) {
                            List<UserDocument> userDocumentsList = (List<UserDocument>) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.DETAILS_USERDOCUMENTS);
                            List<User> unauthorizedUsersList = (List<User>) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.DETAILS_UNAUTHORIZED_USERS);
                            int addUserIndex = (int) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.ADD_USERDOCUMENT_SELECTED_INDEX);
                            User selectedUser = (User) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.ADD_USERDOCUMENT_USER);
                            userDocumentsList.add(new UserDocument(response.getId(), request.getUserId(), selectedUser.getUsername(), selectedUser.getName(), selectedUser.getSurname(), selectedUser.getEmail(), request.isCanView(), request.isCanPrint(), request.isCanDownload(), 0, request.getMaxViewsNumber()));
                            unauthorizedUsersList.remove(addUserIndex);
                            Application.getInstance().getDocumentDetailDialog().loadTablesData();
                            Application.getInstance().getAddUserDocumentDialog().hideDialog();
                        }
                    } catch (Exception ex) {
                        Application.getInstance().getLoadingDialog().hideDialog();
                        Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
                    }
                    
                }).start();
            } catch (Exception ex) {
                Application.getInstance().getLoadingDialog().hideDialog();
                Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
            }
        }
    }
}