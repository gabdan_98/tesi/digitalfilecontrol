/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.controls;

import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.dtos.GetMySharedDocumentsResponse;
import it.unibas.digitalFileControl.dtos.GetSharedWithMeDocumentsResponse;
import it.unibas.digitalFileControl.dtos.AuthTokenResponse;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.model.Enumerator;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

/**
 *
 * @author Gabriele D
 */
public class LoginControl {
    private Action loginAction = new LoginAction();

    public Action getLoginAction() {
        return loginAction;
    }
    
    private class LoginAction extends AbstractAction {
        
        public LoginAction() {
            putValue(Action.NAME, "Login");
            putValue(Action.SHORT_DESCRIPTION, "Effettua il login nell'applicazione");
            putValue(Action.MNEMONIC_KEY, KeyEvent.VK_ENTER);
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("Ctrl L"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Application.getInstance().getLoginFrame().disableLoginButton();
            String username = Application.getInstance().getLoginFrame().getUsername();
            String password = Application.getInstance().getLoginFrame().getPassword();
                    
            Application.getInstance().getLoadingDialog().showDialog();
            new Thread(() -> {
            try {
                
                AuthTokenResponse response = Application.getInstance().getDaoAccount().getAuthTokens(username, password);
                
                if(!response.isSuccess()) {
                    Application.getInstance().getLoadingDialog().hideDialog();
                    Application.getInstance().getLoginFrame().enableLoginButton();
                    if(response.isTooManyRequests()) {
                        Application.getInstance().getFrame().showInfoMessage("Hai effettuato troppi tentativi di login, riprova tra 3 ore");
                        return;
                    }
                    Application.getInstance().getFrame().showInfoMessage("Username o password non validi");
                    return;
                }
                Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.LOGGED_USER, response.getUser());
                Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.ACCESS_TOKEN, response.getAccessToken());
                Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.REFRESH_TOKEN, response.getRefreshToken());
                Application.getInstance().getHttpClient().setAccessToken(response.accessToken);
                GetSharedWithMeDocumentsResponse httpResponse = Application.getInstance().getDaoDocuments().getSharedWithMeDocuments();
                List<Document> sharedWithMeDocuments = httpResponse.getDocuments();
                GetMySharedDocumentsResponse httpResponse2 = Application.getInstance().getDaoDocuments().getMySharedDocuments();
                Application.getInstance().getLoadingDialog().hideDialog();
                List<Document> mySharedDocuments = httpResponse2.getDocuments();
                Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.SHARED_WITH_ME_DOCUMENTS, sharedWithMeDocuments);
                Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.MY_SHARED_DOCUMENTS, mySharedDocuments);
                Application.getInstance().getLoginFrame().hideFrame();
                Application.getInstance().getFrame().showFrame();
            } catch (Exception ex) {
                Application.getInstance().getLoadingDialog().hideDialog();
                Application.getInstance().getFrame().showErrorMessage("Si è verificato un errore");
            }
            }).start();
        }
    }
}