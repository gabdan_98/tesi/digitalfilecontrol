/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.controls;

import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.model.Enumerator;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

/**
 *
 * @author Gabriele D
 */
public class MenuControl {
    private Action exitAction = new ExitAction();
    private Action showProjectInfoAction = new ShowProjectInfoAction();
    private Action logoutAction = new LogoutAction();

    public Action getShowProjectInfoAction() {
        return showProjectInfoAction;
    }

    public Action getExitAction() {
        return exitAction;
    }

    public Action getLogoutAction() {
        return logoutAction;
    }

    private class LogoutAction extends AbstractAction {
        
        public LogoutAction() {
            putValue(Action.NAME, "Logout");
            putValue(Action.SHORT_DESCRIPTION, "Effettua il logout dal tuo account");
            putValue(Action.MNEMONIC_KEY, KeyEvent.VK_L);
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("Ctrl L"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            Application.getInstance().getLocalStorage().putBeans((Enumerator.Storage.ACCESS_TOKEN), null);
            Application.getInstance().getLocalStorage().putBeans((Enumerator.Storage.REFRESH_TOKEN), null);
            Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.MY_SHARED_DOCUMENTS, null);
            Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.VIEW_SELECTED_DOCUMENT, null);
            Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.DETAIL_SELECTED_DOCUMENT, null);
            Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.SHARED_WITH_ME_DOCUMENTS, null);
            Application.getInstance().getFrame().hideFrame();
            Application.getInstance().getLoginFrame().showFrame();
        }
    }
    
    private class ExitAction extends AbstractAction {
        
        public ExitAction() {
            putValue(Action.NAME, "Esci dall'app");
            putValue(Action.SHORT_DESCRIPTION, "Esci dall'applicazione");
            putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("Ctrl E"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
    
    private class ShowProjectInfoAction extends AbstractAction {
        
        public ShowProjectInfoAction() {
            putValue(Action.NAME, "Info");
            putValue(Action.SHORT_DESCRIPTION, "Informazioni sul progetto");
            putValue(Action.MNEMONIC_KEY, KeyEvent.VK_I);
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("Ctrl I"));
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            Application.getInstance().getProjectInfoDialog().showDialog();
        }
    }
}
