/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package it.unibas.digitalFileControl.views;

import it.unibas.digitalFileControl.Application;

/**
 *
 * @author Gabriele D
 */
public class AddUserDocumentDialog extends javax.swing.JDialog {

    /**
     * Creates new form AddUserDocumentDialog
     */
    public AddUserDocumentDialog(java.awt.Frame parent) {
        super(parent, true);
    }
    
    public void init() {
        initComponents();
        this.pack();
        this.setLocationRelativeTo(getParent());
        setActions();
        this.viewCheckbox.setText("Visualizzazione");
        this.printCheckbox.setText("Stampa");
        this.downloadCheckbox.setText("Download");
        this.maxViewsCheckbox.setText("Limite di visualizzazioni");
    }
    
    public void showDialog() {
        this.viewCheckbox.setSelected(false);
        this.printCheckbox.setSelected(false);
        this.downloadCheckbox.setSelected(false);
        this.maxViewsCheckbox.setSelected(false);
        this.maxViewNumberSpinner.setEnabled(false);
        this.maxViewNumberSpinner.setValue(0);
        this.setVisible(true);
    }
    
    public void hideDialog() {
        this.setVisible(false);
    }
    
    public boolean isCanView() {
        return this.viewCheckbox.isSelected();
    }
    
    public boolean isCanPrint() {
        return this.printCheckbox.isSelected();
    }
    
    public boolean isCanDownload() {
        return this.downloadCheckbox.isSelected();
    }
    
    public boolean isViewsLimit() {
        return this.maxViewsCheckbox.isSelected();
    }
    
    public Integer getMaxViewsNumber() {
        return (Integer)this.maxViewNumberSpinner.getValue();
    }
    
    public void changeMaxViewsSpinnerState() {
        this.maxViewNumberSpinner.setEnabled(!this.maxViewNumberSpinner.isEnabled());
    }
    
    public void setCanViewCheckboxSelected() {
        this.viewCheckbox.setSelected(true);
    }
    
    public void setCanViewCheckboxUnSelected() {
        this.viewCheckbox.setSelected(false);
    }
    
    public void setCanPrintCheckboxUnselected() {
        this.printCheckbox.setSelected(false);
    }
    
    public void setCanDownloadCheckboxUnselected() {
        this.downloadCheckbox.setSelected(false);
    }
    
    public void setViewsLimitCheckboxUnselected() {
        this.maxViewsCheckbox.setSelected(false);
        this.maxViewNumberSpinner.setEnabled(false);
    }
    
    public void setViewsLimitSpinnerEnabled() {
        this.maxViewNumberSpinner.setEnabled(true);
    }
    
    public void setActions() {
        this.saveButton.setAction(Application.getInstance().getAddUserDocumentControl().getSaveUserDocumentAction());
        this.maxViewsCheckbox.setAction(Application.getInstance().getAddUserDocumentControl().getChangeSpinnerStateAction());
        this.viewCheckbox.setAction(Application.getInstance().getAddUserDocumentControl().getViewCheckboxSelectionAction());
        this.printCheckbox.setAction(Application.getInstance().getAddUserDocumentControl().getCheckboxSelectionAction());
        this.downloadCheckbox.setAction(Application.getInstance().getAddUserDocumentControl().getCheckboxSelectionAction());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        saveButton = new javax.swing.JButton();
        viewCheckbox = new javax.swing.JCheckBox();
        printCheckbox = new javax.swing.JCheckBox();
        downloadCheckbox = new javax.swing.JCheckBox();
        maxViewsCheckbox = new javax.swing.JCheckBox();
        maxViewNumberSpinner = new javax.swing.JSpinner();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Aggiunta utente a documento");

        saveButton.setText("Salva");
        saveButton.setFocusable(false);

        viewCheckbox.setText("Visualizzazione");
        viewCheckbox.setFocusable(false);

        printCheckbox.setText("Stampa");
        printCheckbox.setFocusable(false);

        downloadCheckbox.setText("Download");
        downloadCheckbox.setFocusable(false);

        maxViewsCheckbox.setText("Limite di visualizzazioni");
        maxViewsCheckbox.setFocusable(false);

        maxViewNumberSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        maxViewNumberSpinner.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(maxViewsCheckbox)
                        .addGap(26, 26, 26)
                        .addComponent(maxViewNumberSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(downloadCheckbox)
                    .addComponent(printCheckbox)
                    .addComponent(viewCheckbox)
                    .addComponent(saveButton))
                .addContainerGap(216, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(viewCheckbox)
                .addGap(28, 28, 28)
                .addComponent(printCheckbox)
                .addGap(29, 29, 29)
                .addComponent(downloadCheckbox)
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(maxViewsCheckbox)
                    .addComponent(maxViewNumberSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                .addComponent(saveButton)
                .addGap(34, 34, 34))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox downloadCheckbox;
    private javax.swing.JSpinner maxViewNumberSpinner;
    private javax.swing.JCheckBox maxViewsCheckbox;
    private javax.swing.JCheckBox printCheckbox;
    private javax.swing.JButton saveButton;
    private javax.swing.JCheckBox viewCheckbox;
    // End of variables declaration//GEN-END:variables
}
