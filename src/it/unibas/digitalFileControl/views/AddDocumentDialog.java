/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package it.unibas.digitalFileControl.views;

import it.unibas.digitalFileControl.Application;
import java.util.Date;
import javax.swing.JFrame;

/**
 *
 * @author Gabriele D
 */
public class AddDocumentDialog extends javax.swing.JDialog {

    /**
     * Creates new form AddDocumentDialog
     */
    public AddDocumentDialog(java.awt.Frame parent) {
        super(parent, true);
    }
    
    public void init() {
        initComponents();
        this.pack();
        this.setLocationRelativeTo(getParent());
        setActions();
    }
    
    public void initDisabled() {
        this.level2Radio.setSelected(false);
        this.uploadFileLevel2.setEnabled(false);
        this.allowPrintLevel2.setEnabled(false);
        this.allowCopyLevel2.setEnabled(false);
        this.labelPassword.setEnabled(false);
        this.password.setEnabled(false);
        
        this.level3Radio.setSelected(false);
        this.uploadFileLevel3.setEnabled(false);
        this.documentName.setEnabled(false);
        this.labelDocumentName.setEnabled(false);
        this.documentDescription.setEnabled(false);
        this.labelDocumentDescription.setEnabled(false);
        this.labelExpirationDate.setEnabled(false);
        this.expirationDateSpinner.setEnabled(false);
        
        this.level1Radio.setSelected(false);
        this.uploadFileLevel1.setEnabled(false);
        this.allowPrintLevel1.setEnabled(false);
        this.allowCopyLevel1.setEnabled(false);
        this.labelExpirationDate.setSelected(false);
    }
    
    public void showDialog() {
        initDisabled();
        this.setVisible(true);
    }
    
    public void hideDialog() {
        this.setVisible(false);
    }
    
    public boolean isLevel1Selected() {
        return this.level1Radio.isSelected();
    }
    
    public boolean isLevel2Selected() {
        return this.level2Radio.isSelected();
    }
    
    public boolean isLevel3Selected() {
        return this.level3Radio.isSelected();
    }
    
    public void setLevel1CheckboxSelected(boolean value) {
        this.level1Radio.setSelected(value);
    }
    
    public void setLevel1Selection() {
        this.level2Radio.setSelected(false);
        this.uploadFileLevel2.setEnabled(false);
        this.allowPrintLevel2.setEnabled(false);
        this.allowCopyLevel2.setEnabled(false);
        this.labelPassword.setEnabled(false);
        this.password.setEnabled(false);
        
        this.level3Radio.setSelected(false);
        this.uploadFileLevel3.setEnabled(false);
        this.documentName.setEnabled(false);
        this.labelDocumentName.setEnabled(false);
        this.documentDescription.setEnabled(false);
        this.labelDocumentDescription.setEnabled(false);
        this.labelExpirationDate.setEnabled(false);
        this.expirationDateSpinner.setEnabled(false);
        
        this.uploadFileLevel1.setEnabled(true);
        this.allowPrintLevel1.setEnabled(true);
        this.allowCopyLevel1.setEnabled(true);
    }
    
    public void setLevel2Selection() {
        this.level1Radio.setSelected(false);
        this.uploadFileLevel1.setEnabled(false);
        this.allowPrintLevel1.setEnabled(false);
        this.allowCopyLevel1.setEnabled(false);
        
        this.level3Radio.setSelected(false);
        this.uploadFileLevel3.setEnabled(false);
        this.documentName.setEnabled(false);
        this.labelDocumentName.setEnabled(false);
        this.documentDescription.setEnabled(false);
        this.labelDocumentDescription.setEnabled(false);
        this.expirationDateSpinner.setEnabled(false);
        this.labelExpirationDate.setEnabled(false);
        
        this.uploadFileLevel2.setEnabled(true);
        this.allowPrintLevel2.setEnabled(true);
        this.allowCopyLevel2.setEnabled(true);
        this.labelPassword.setEnabled(true);
        this.password.setEnabled(true);
    }
    
    public void setLevel3Selection() {
        this.level1Radio.setSelected(false);
        this.uploadFileLevel1.setEnabled(false);
        this.allowPrintLevel1.setEnabled(false);
        this.allowCopyLevel1.setEnabled(false);
        
        this.level2Radio.setSelected(false);
        this.uploadFileLevel2.setEnabled(false);
        this.allowPrintLevel2.setEnabled(false);
        this.allowCopyLevel2.setEnabled(false);
        this.labelPassword.setEnabled(false);
        this.password.setEnabled(false);

        this.uploadFileLevel3.setEnabled(true);
        this.documentName.setEnabled(true);
        this.labelDocumentName.setEnabled(true);
        this.documentDescription.setEnabled(true);
        this.labelDocumentDescription.setEnabled(true);
        this.labelExpirationDate.setEnabled(true);
    }
    
    public void setLevel1Deselection () {
        this.level1Radio.setSelected(false);
        this.uploadFileLevel1.setEnabled(false);
        this.allowPrintLevel1.setEnabled(false);
        this.allowCopyLevel1.setEnabled(false);
    }
    
    public void setLevel2Deselection() {
        this.level2Radio.setSelected(false);
        this.uploadFileLevel2.setEnabled(false);
        this.allowPrintLevel2.setEnabled(false);
        this.allowCopyLevel2.setEnabled(false);
        this.labelPassword.setEnabled(false);
        this.password.setEnabled(false);
    }
    
    public void setLevel3Deselection() {
        this.level3Radio.setSelected(false);
        this.uploadFileLevel3.setEnabled(false);
        this.documentName.setEnabled(false);
        this.labelDocumentName.setEnabled(false);
        this.documentDescription.setEnabled(false);
        this.labelDocumentDescription.setEnabled(false);
        this.expirationDateSpinner.setEnabled(false);
        this.labelExpirationDate.setEnabled(false);
    }
    
    public void changeExpirationDateSpinnerState() {
        this.expirationDateSpinner.setEnabled(!this.expirationDateSpinner.isEnabled());
    }
    
    public String getDocumentName() {
        return this.documentName.getText();
    }
    
    public String getDocumentDescription() {
        return this.documentDescription.getText();
    }
    
    public boolean isDocumentExpire() {
        return this.labelExpirationDate.isSelected();
    }
    
    public Date getDocumentExpirationDate() {
        try {
            this.expirationDateSpinner.commitEdit();
        } catch (Exception ex) {
            
        }
        
        return (Date) this.expirationDateSpinner.getValue();
    }
    
    public boolean isLevel1AllowPrint() {
        return allowPrintLevel1.isSelected();
    }
    
    public boolean isLevel1AllowCopy() {
        return allowCopyLevel1.isSelected();
    }
    
    public boolean isLevel2AllowPrint() {
        return allowPrintLevel2.isSelected();
    }
    
    public boolean isLevel2AllowCopy() {
        return allowCopyLevel2.isSelected();
    }
    
    public String getDocumentPassword() {
        return password.getText();
    }
    
    private void setActions() {
        this.uploadFileLevel1.setAction(Application.getInstance().getAddDocumentControl().getUploadFileAction());
        this.uploadFileLevel2.setAction(Application.getInstance().getAddDocumentControl().getUploadFileAction());
        this.uploadFileLevel3.setAction(Application.getInstance().getAddDocumentControl().getUploadFileAction());
        this.protectDocumentButton.setAction((Application.getInstance().getAddDocumentControl().getProtectDocumentAction()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        jLabel1 = new javax.swing.JLabel();
        uploadFileLevel1 = new javax.swing.JButton();
        uploadFileLevel2 = new javax.swing.JButton();
        uploadFileLevel3 = new javax.swing.JButton();
        allowPrintLevel1 = new javax.swing.JCheckBox();
        allowCopyLevel1 = new javax.swing.JCheckBox();
        allowPrintLevel2 = new javax.swing.JCheckBox();
        allowCopyLevel2 = new javax.swing.JCheckBox();
        password = new javax.swing.JTextField();
        labelPassword = new javax.swing.JLabel();
        documentName = new javax.swing.JTextField();
        documentDescription = new javax.swing.JTextField();
        labelDocumentName = new javax.swing.JLabel();
        labelDocumentDescription = new javax.swing.JLabel();
        expirationDateSpinner = new javax.swing.JSpinner();
        protectDocumentButton = new javax.swing.JButton();
        level1Radio = new javax.swing.JRadioButton();
        level2Radio = new javax.swing.JRadioButton();
        level3Radio = new javax.swing.JRadioButton();
        labelExpirationDate = new javax.swing.JCheckBox();

        jScrollPane1.setViewportView(jEditorPane1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Seleziona il livello di sicurezza desiderato");

        uploadFileLevel1.setText("Carica file");
        uploadFileLevel1.setFocusable(false);

        uploadFileLevel2.setText("Carica file");
        uploadFileLevel2.setFocusable(false);

        uploadFileLevel3.setText("Carica file");
        uploadFileLevel3.setActionCommand("Carica file");
        uploadFileLevel3.setFocusable(false);

        allowPrintLevel1.setText("Consenti stampa");
        allowPrintLevel1.setFocusable(false);

        allowCopyLevel1.setText("Consenti copia");
        allowCopyLevel1.setFocusable(false);

        allowPrintLevel2.setText("Consenti stampa");
        allowPrintLevel2.setFocusable(false);

        allowCopyLevel2.setText("Consenti copia");
        allowCopyLevel2.setFocusable(false);

        password.setToolTipText("Password");

        labelPassword.setText("Password");
        labelPassword.setFocusable(false);

        labelDocumentName.setText("Nome documento");
        labelDocumentName.setFocusable(false);

        labelDocumentDescription.setText("Descrizione documento");
        labelDocumentDescription.setFocusable(false);

        expirationDateSpinner.setModel(new javax.swing.SpinnerDateModel(new java.util.Date(), new java.util.Date(), null, java.util.Calendar.DAY_OF_MONTH));

        protectDocumentButton.setText("Proteggi documento");
        protectDocumentButton.setFocusable(false);

        level1Radio.setAction(Application.getInstance().getAddDocumentControl().getLevel1SelectionAction());
        level1Radio.setText("Livello 1 - SICUREZZA BASSA (Aggiunta restrizioni stampa/copia)");
        level1Radio.setFocusable(false);

        level2Radio.setAction(Application.getInstance().getAddDocumentControl().getLevel2SelectionAction());
        level2Radio.setText("Livello 2 - SICUREZZA MEDIA (Aggiunta restrizioni stampa/copia + password)");
        level2Radio.setFocusable(false);

        level3Radio.setAction(Application.getInstance().getAddDocumentControl().getLevel3SelectionAction());
        level3Radio.setText("Livello 3 - SICUREZZA MASSIMA (Condivisione file sicura tramite cloud con controllo accessi)");
        level3Radio.setFocusable(false);

        labelExpirationDate.setAction(Application.getInstance().getAddDocumentControl().getEnableDocumentExpirationAction());
        labelExpirationDate.setText("Imposta scadenza");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(protectDocumentButton)
                .addGap(41, 41, 41))
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(level3Radio)
                    .addComponent(level2Radio)
                    .addComponent(jLabel1)
                    .addComponent(level1Radio)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(144, 144, 144)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(documentName, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(labelDocumentName))
                                .addGap(27, 27, 27)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelDocumentDescription)
                                    .addComponent(documentDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(uploadFileLevel3)))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(expirationDateSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelExpirationDate)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(uploadFileLevel1)
                                .addGap(28, 28, 28)
                                .addComponent(allowPrintLevel1)
                                .addGap(41, 41, 41)
                                .addComponent(allowCopyLevel1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(uploadFileLevel2)
                                .addGap(28, 28, 28)
                                .addComponent(allowPrintLevel2)
                                .addGap(41, 41, 41)
                                .addComponent(allowCopyLevel2)
                                .addGap(43, 43, 43)
                                .addComponent(labelPassword)
                                .addGap(18, 18, 18)
                                .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(70, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel1)
                .addGap(28, 28, 28)
                .addComponent(level1Radio)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uploadFileLevel1)
                    .addComponent(allowPrintLevel1)
                    .addComponent(allowCopyLevel1))
                .addGap(40, 40, 40)
                .addComponent(level2Radio)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uploadFileLevel2)
                    .addComponent(allowPrintLevel2)
                    .addComponent(allowCopyLevel2)
                    .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPassword))
                .addGap(41, 41, 41)
                .addComponent(level3Radio)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uploadFileLevel3)
                    .addComponent(labelDocumentName)
                    .addComponent(labelDocumentDescription)
                    .addComponent(labelExpirationDate))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(documentName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(documentDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(expirationDateSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addComponent(protectDocumentButton)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox allowCopyLevel1;
    private javax.swing.JCheckBox allowCopyLevel2;
    private javax.swing.JCheckBox allowPrintLevel1;
    private javax.swing.JCheckBox allowPrintLevel2;
    private javax.swing.JTextField documentDescription;
    private javax.swing.JTextField documentName;
    private javax.swing.JSpinner expirationDateSpinner;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelDocumentDescription;
    private javax.swing.JLabel labelDocumentName;
    private javax.swing.JCheckBox labelExpirationDate;
    private javax.swing.JLabel labelPassword;
    private javax.swing.JRadioButton level1Radio;
    private javax.swing.JRadioButton level2Radio;
    private javax.swing.JRadioButton level3Radio;
    private javax.swing.JTextField password;
    private javax.swing.JButton protectDocumentButton;
    private javax.swing.JButton uploadFileLevel1;
    private javax.swing.JButton uploadFileLevel2;
    private javax.swing.JButton uploadFileLevel3;
    // End of variables declaration//GEN-END:variables
}
