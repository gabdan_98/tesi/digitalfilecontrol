package it.unibas.digitalFileControl.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.Document;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Gabriele D
 */
public class ConfigurationHelper {
   
    private Map<String, Object> configuration = new HashMap<>();
    
    public void loadConfiguration() {
        try {
            Path filePath = Path.of("src/configuration.json");
            String fileContent = Files.readString(filePath);
            JsonElement jsonElement = JsonParser.parseString(fileContent);
            JsonObject jsonObj = jsonElement.getAsJsonObject();
            String value = jsonObj.get("webServiceURL").toString();
            configuration.put(Enumerator.Configuration.WEB_SERVICE_URL, getCleanedValue(value));
        } catch (Exception ex) {
            
        }
    }
    
    private String getCleanedValue(String value) {
        return value.replace("\"", "");
    }
    
    public Object getValue(String key) {
        return configuration.get(key);
    }
}