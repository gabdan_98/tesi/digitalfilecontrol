package it.unibas.digitalFileControl.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.dtos.AuthTokenResponse;
import it.unibas.digitalFileControl.model.Enumerator;
import it.unibas.digitalFileControl.model.HttpResponse;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Gabriele D
 */
public class HttpClient {
    
    private String ACCESS_TOKEN;
    
    public void setAccessToken(String accessToken) {
        this.ACCESS_TOKEN = accessToken;
    }
    
    public HttpResponse executeGet(String targetUrl) {
        return executeGenericRequest(targetUrl, null, Enumerator.HttpRequestTypes.GET);
    }
    
    public HttpResponse executePost(String targetUrl, JsonObject urlParameters) {
        return executeGenericRequest(targetUrl, urlParameters, Enumerator.HttpRequestTypes.POST);
    }
    
    public HttpResponse executePut(String targetUrl, JsonObject urlParameters) {
        return executeGenericRequest(targetUrl, urlParameters, Enumerator.HttpRequestTypes.PUT);
    }
    
    public HttpResponse executeDelete(String targetUrl, JsonObject urlParameters) {
        return executeGenericRequest(targetUrl, urlParameters, Enumerator.HttpRequestTypes.DELETE);
    }
    
    private HttpResponse executeGenericRequest(String targetUrl, JsonObject urlParameters, String requestType) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(targetUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(requestType);
            
            if((requestType.equalsIgnoreCase(Enumerator.HttpRequestTypes.POST) || requestType.equalsIgnoreCase(Enumerator.HttpRequestTypes.PUT) || requestType.equalsIgnoreCase(Enumerator.HttpRequestTypes.DELETE)) && urlParameters != null) {
                connection.setUseCaches(false);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.size()));
                connection.setRequestProperty("Content-Language", "it-IT");
                connection.setRequestProperty("Accept-Charset", "UTF-8");
                connection.setRequestProperty("charset", "UTF-8");
            }
            
            String authToken = ACCESS_TOKEN;
            if(authToken != null) {
                StringBuilder headerValue = new StringBuilder();
                headerValue.append("Bearer ");
                headerValue.append(authToken);
                connection.setRequestProperty("Authorization", headerValue.toString());
                connection.setRequestProperty("ViewerSecurity", "jsKgs_DG*5s2S@_SGW25ygH6");
            }
            
            if((requestType.equalsIgnoreCase(Enumerator.HttpRequestTypes.POST) || requestType.equalsIgnoreCase(Enumerator.HttpRequestTypes.PUT) || requestType.equalsIgnoreCase(Enumerator.HttpRequestTypes.DELETE)) && urlParameters != null) {
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.write(urlParameters.toString().getBytes("UTF-8"));
                wr.close();
            }

            int responseCode = connection.getResponseCode();
            
            if(responseCode == 401 && Application.getInstance() != null) {
                AuthTokenResponse tokens = Application.getInstance().getDaoAccount().refreshTokens((String) Application.getInstance().getLocalStorage().getBeans(Enumerator.Storage.REFRESH_TOKEN));
                Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.ACCESS_TOKEN, tokens.getAccessToken());
                Application.getInstance().getLocalStorage().putBeans(Enumerator.Storage.REFRESH_TOKEN, tokens.getRefreshToken());
                ACCESS_TOKEN = tokens.getAccessToken();
                return executeGenericRequest(targetUrl, urlParameters, requestType);
            }

            if(responseCode == 200) {
                // Get response
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String line;
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                return new HttpResponse(responseCode, response.toString());
            } else {
                try {
                    String responseString = null;
                    InputStream stream = connection.getErrorStream();
                    if(stream != null) {
                        BufferedReader rd = new BufferedReader(new InputStreamReader(stream));
                        StringBuilder response = new StringBuilder();
                        String line;
                        while((line = rd.readLine()) != null) {
                            response.append(line);
                            response.append('\r');
                        }
                        rd.close();
                        responseString = response.toString();
                    }
                    return new HttpResponse(connection.getResponseCode(), responseString);
                } catch (Exception ex2) {
                
                }
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (Exception ex) {
            String m = ex.getMessage();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }
}