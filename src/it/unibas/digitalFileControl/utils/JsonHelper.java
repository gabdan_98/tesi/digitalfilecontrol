/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.persistence.CustomDAOServiceResponse;
import it.unibas.digitalFileControl.model.Document;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 *
 * @author Gabriele D
 */
public class JsonHelper {
    
    public static CustomDAOServiceResponse parseJson(String response, Type type, int statusCode) throws Exception {
        CustomDAOServiceResponse result = null;
        if(response != null && response != "") {
            JsonElement jelement = JsonParser.parseString(response);
            Gson gson = new Gson();
            result = gson.fromJson(jelement, type);
        } else {
            try {
                Class c = Class.forName(type.getTypeName());
                result = (CustomDAOServiceResponse)c.newInstance();
            } catch (Exception ex) {
                throw new Exception("Errore");
            }
        }
        result.setStatusCode(statusCode);
        if(statusCode == 500) {
            throw new Exception("Errore");
        }
        return result;
    }
}