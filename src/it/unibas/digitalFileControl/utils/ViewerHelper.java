/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.utils;

import it.unibas.digitalFileControl.Application;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.PDimension;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.pobjects.graphics.WatermarkCallback;
import org.icepdf.core.util.GraphicsRenderingHints;
import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;
import org.icepdf.ri.util.PropertiesManager;

/**
 *
 * @author Gabriele D
 */
public class ViewerHelper {
    public static void openPDFDocument(String base64, boolean canView, boolean canPrint, boolean canDownload) {
        byte[] decoder = Base64.getDecoder().decode(base64);
        SwingController controller = new SwingController();
        PropertiesManager properties = new PropertiesManager(System.getProperties(), ResourceBundle.getBundle(PropertiesManager.DEFAULT_MESSAGE_BUNDLE));
        properties.setBoolean(PropertiesManager.PROPERTY_SHOW_UTILITY_SAVE, canView);
        properties.setBoolean(PropertiesManager.PROPERTY_SHOW_UTILITY_PRINT, canPrint);
        properties.setBoolean(PropertiesManager.PROPERTY_SHOW_UTILITY_SAVE, canDownload);
        SwingViewBuilder factory = new SwingViewBuilder(controller, properties);
        JPanel viewerComponentPanel = factory.buildViewerPanel();
        controller.openDocument(decoder, 0, base64.length(), "Document", "Document");
        
        // setup two threads to handle image extraction.
        /*ExecutorService executorService = Executors.newFixedThreadPool(4);
        try {
            executorService.shutdown();
            Document icePdfDocument = controller.getDocument();
            icePdfDocument.setWatermarkCallback(new MyWatermarkCallback());
            int pages = icePdfDocument.getNumberOfPages();
            java.util.List<Callable<Void>> callables = new ArrayList<Callable<Void>>(pages);
            for (int i = 0; i <= pages; i++) {
                callables.add(new CapturePage(icePdfDocument, i));
            }
            executorService.invokeAll(callables);
            executorService.submit(new DocumentCloser(icePdfDocument)).get();
        } catch (InterruptedException e) {
            System.out.println("Error parsing PDF document " + e);
        } catch (ExecutionException e) {
            System.out.println("Error parsing PDF document " + e);
        }
        executorService.shutdown();*/

        Application.getInstance().getPdfViewerFrame().setContentPane(viewerComponentPanel);
        Application.getInstance().getPdfViewerFrame().showFrame();
    }
    
    public void closePDFViewer() {
        Application.getInstance().getPdfViewerFrame().setVisible(false);
    }
    
    public class MyWatermarkCallback implements WatermarkCallback {
        // to avoid memory leaks be careful not to save an instance of page in your implementation
        public void paintWatermark(Graphics g, Page page, int renderHintType, int boundary, float userRotation, float userZoom) {
            Graphics2D g2 = (Graphics2D) g;

            // setup the graphics context and a 45 degree rotation effect.
            Rectangle2D.Float mediaBox = page.getPageBoundary(boundary);
            AffineTransform af = new AffineTransform();
            af.scale(1, -1);
            af.rotate(-45.0 * Math.PI / 180.0, mediaBox.getWidth() / 2.0, -mediaBox
                    .getHeight() / 2.0);
            g2.transform(af);

            // apply transparency
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.75f));
            // ICEpdf red.
            g2.setColor(new Color(186, 0, 0));
            // draw Some text.
            String footerText = "This Page " + (page.getPageIndex() + 1);
            g2.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 36));
            FontMetrics fontMetrics = g2.getFontMetrics();
            Rectangle2D fontBounds = fontMetrics.getStringBounds(footerText.toCharArray(),
                    0, footerText.length(),
                    g2);

            int x = (int) (mediaBox.x + (mediaBox.width - fontBounds.getWidth()) / 2.0);
            int y = -(int) (mediaBox.y - (mediaBox.height - fontBounds.getHeight()) / 2.0);
            g2.drawString(footerText, x, y);
        }
    }
    
    public class CapturePage implements Callable<Void> {
        private Document document;
        private int pageNumber;
        private float scale = 1f;
        private float rotation = 0f;

        private CapturePage(Document document, int pageNumber) {
            this.document = document;
            this.pageNumber = pageNumber;
        }

        public Void call() {
            try {
                Page page = document.getPageTree().getPage(pageNumber);
                page.init();
                PDimension sz = page.getSize(Page.BOUNDARY_CROPBOX, rotation, scale);

                int pageWidth = (int) sz.getWidth();
                int pageHeight = (int) sz.getHeight();

                BufferedImage image = new BufferedImage(pageWidth,
                        pageHeight,
                        BufferedImage.TYPE_INT_RGB);
                Graphics g = image.createGraphics();

                page.paint(g, GraphicsRenderingHints.PRINT,
                        Page.BOUNDARY_CROPBOX, rotation, scale);
                g.dispose();
                // capture the page image to file

                System.out.println("Capturing page " + pageNumber);
                File file = new File("imageCapture_" + pageNumber + ".png");
                ImageIO.write(image, "png", file);
                image.flush();
            } catch (Throwable e) {
                e.printStackTrace();
            }

            return null;
        }
    }
    
    public class DocumentCloser implements Callable<Void> {
        private Document document;

        private DocumentCloser(Document document) {
            this.document = document;
        }

        public Void call() {
            if (document != null) {
                document.dispose();
                System.out.println("Document disposed");
            }
            return null;
        }
    }
}