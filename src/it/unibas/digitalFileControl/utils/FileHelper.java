/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.utils;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author Gabriele D
 */
public class FileHelper {
    
    public static String readTextFromFile(String path) {
        StringBuilder sb = new StringBuilder();
        try
        {
           BufferedReader br = new BufferedReader(new FileReader(path));
           String st;
           while ((st = br.readLine()) != null) {
               sb.append(st);
           }
        }
        catch (Exception ex) {

        }
        
        return sb.toString();
    }
}