/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.utils;

import it.unibas.digitalFileControl.exception.EncryptionException;
import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Gabriele D
 */
public class EncryptionHelper {
    private static final String SECRET_KEY = "I2?_S*/SG2sg!+sèS24@sfe3Tt£t23h_S-&%DG2?";
    private static final String SALT = "NKdsg_?!SDe5e*fhfe@@SFSde£$gdg";
    private static final byte[] IV = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    
    public static String decrypt(String encryptedString) throws EncryptionException {
        try
        {
            final String password = "I2?_S*/SG2sg!+sèS24@sfe3Tt£t23h_S-&%DG2?";
            final String iv = "16806642kbM7c5!$";
            byte[] salt = new byte[] { 34, (byte) 134, (byte) 145, 12, 7, 6, (byte) 243, 63, 43, 54, 75, 65, 53, 2, 34, 54,
                    45, 67, 64, 64, 32, (byte) 213 };

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 1000, 256);
            SecretKey tmp = factory.generateSecret(spec);

            SecretKeySpec secret = new SecretKeySpec(tmp.getEncoded(), "AES");
            IvParameterSpec ivs = new IvParameterSpec(iv.getBytes(StandardCharsets.US_ASCII));

             // your code use PKCS7, but I use PKCS5 because it shows exception in my case
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secret, ivs);

            // base64 string from C# output
            String plaintext = new String(cipher.doFinal(Base64.getDecoder().decode(encryptedString)), "UTF-8");
            return plaintext;
        }
        catch (Exception ex) {
            throw new EncryptionException("Error while decrypting string");
        }
    }
}
