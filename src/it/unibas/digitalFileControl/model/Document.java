/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.model;

import java.util.Date;

/**
 *
 * @author Gabriele D
 */
public class Document {
    private int userDocumentId;
    private int id;
    private String name;
    private String description;
    private int documentTypeId;
    private boolean canView;
    private boolean canPrint;
    private boolean canDownload;
    private Date creationTime;
    private Date expirationTime;
    private String content;
    private String phisicalName;
    private String sharedBy;

    public Document() {

    }

    public Document(int userDocumentId, int id, String name, String description, int documentTypeId, boolean canView, boolean canPrint, boolean canDownload, Date creationTime, Date expirationTime, String content, String phisicalName) {
        this.userDocumentId = userDocumentId;
        this.id = id;
        this.name = name;
        this.description = description;
        this.documentTypeId = documentTypeId;
        this.canView = canView;
        this.canPrint = canPrint;
        this.canDownload = canDownload;
        this.creationTime = creationTime;
        this.expirationTime = expirationTime;
        this.content = content;
        this.phisicalName = phisicalName;
    }

    public Date getCreationDate() {
        return creationTime;
    }

    public void setCreationDate(Date creationDate) {
        this.creationTime = creationDate;
    }

    public String getPhisicalName() {
        return phisicalName;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getSharedBy() {
        return sharedBy;
    }

    public void setSharedBy(String sharedBy) {
        this.sharedBy = sharedBy;
    }

    public void setPhisicalName(String phisicalName) {
        this.phisicalName = phisicalName;
    }

    public boolean isCanView() {
        return canView;
    }

    public void setCanView(boolean canView) {
        this.canView = canView;
    }

    public boolean isCanPrint() {
        return canPrint;
    }

    public void setCanPrint(boolean canPrint) {
        this.canPrint = canPrint;
    }

    public boolean isCanDownload() {
        return canDownload;
    }

    public void setCanDownload(boolean canDownload) {
        this.canDownload = canDownload;
    }
    
    public String getName() {
        return name;
    }

    public int getUserDocumentId() {
        return userDocumentId;
    }

    public void setUserDocumentId(int userDocumentId) {
        this.userDocumentId = userDocumentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(int documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = expirationTime;
    }
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
