/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Gabriele D
 */
public class MySharedTableModel extends AbstractTableModel {
    private List<Document> documentList = new ArrayList<Document>();

    public void setDocumentList(List<Document> documentList) {
        this.documentList = documentList;
    }

    @Override
    public int getRowCount() {
        if(documentList == null) {
            return 0;
        }
        return documentList.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(documentList == null) {
            return "";
        }
        Document file = documentList.get(rowIndex);
        if(columnIndex == 0) {
            return file.getName();
        }
        else if(columnIndex == 1) {
            return file.getDescription();
        }
        else if(columnIndex == 2) {
            if(file.getDocumentTypeId()== Enumerator.FileTypes.PDF) {
                return "PDF";
            }
        }
        else if(columnIndex == 3) {
            if(file.getExpirationTime()== null) {
                return "";
            }
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            return formatter.format(file.getExpirationTime());
        }
        return "";
    }

    @Override
    public String getColumnName(int column) {
        if(column == 0) {
            return "Nome";
        }
        else if(column == 1) {
            return "Descrizione";
        }
        else if(column == 2) {
            return "Tipo";
        }
        else if(column == 3) {
            return "Scadenza";
        }
        return "";
    }
    
    public void reloadTable() {
        this.fireTableDataChanged();
    }
    
    public Document getDocumentByIndex(int index) {
        return this.documentList.get(index);
    }
}