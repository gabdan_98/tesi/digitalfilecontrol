/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Gabriele D
 */
public class UnauthorizedUsersTableModel extends AbstractTableModel {
    private List<User> usersList = new ArrayList<User>();
    
    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    @Override
    public int getRowCount() {
        if(usersList == null) {
            return 0;
        }
        return usersList.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(usersList == null) {
            return "";
        }
        User user = usersList.get(rowIndex);
        if(columnIndex == 0) {
            return user.getUsername();
        }
        else if(columnIndex == 1) {
            return user.getName();
        }
        else if(columnIndex == 2) {
            return user.getSurname();
        }
        else if(columnIndex == 3) {
            return user.getEmail();
        }
        return "";
    }

    @Override
    public String getColumnName(int column) {
        if(column == 0) {
            return "Username";
        }
        else if(column == 1) {
            return "Nome";
        }
        else if(column == 2) {
            return "Cognome";
        }
        else if(column == 3) {
            return "Email";
        }
        return "";
    }
    
    public void reloadTable() {
        this.fireTableDataChanged();
    }
    
    public User getUserDocumentByIndex(int index) {
        return this.usersList.get(index);
    }
}
