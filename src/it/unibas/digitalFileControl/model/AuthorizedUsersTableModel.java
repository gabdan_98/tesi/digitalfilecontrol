/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Gabriele D
 */
public class AuthorizedUsersTableModel extends AbstractTableModel {
    private List<UserDocument> userDocumentsList = new ArrayList<UserDocument>();
    
    public void setUserDocumentsList(List<UserDocument> userDocumentsList) {
        this.userDocumentsList = userDocumentsList;
    }

    @Override
    public int getRowCount() {
        if(userDocumentsList == null) {
            return 0;
        }
        return userDocumentsList.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(userDocumentsList == null) {
            return "";
        }
        UserDocument userDocument = userDocumentsList.get(rowIndex);
        if(columnIndex == 0) {
            return userDocument.getUsername();
        }
        else if(columnIndex == 1) {
            if(userDocument.isCanView()) {
                return "Sì";
            } else {
                return "No";
            }
        }
        else if(columnIndex == 2) {
            if(userDocument.isCanPrint()){
                return "Sì";
            } else {
                return "No";
            }
        }
        else if(columnIndex == 3) {
            if(userDocument.isCanDownload()) {
                return "Sì";
            } else {
                return "No";
            }
        }
        else if(columnIndex == 4) {
            return userDocument.getViewsNumber();
        }
        else if(columnIndex == 5) {
            return userDocument.getMaxViewsNumber();
        }
        return "";
    }

    @Override
    public String getColumnName(int column) {
        if(column == 0) {
            return "Username";
        }
        else if(column == 1) {
            return "Visualizzazione";
        }
        else if(column == 2) {
            return "Stampa";
        }
        else if(column == 3) {
            return "Download";
        }
        else if(column == 4) {
            return "Visualizzazioni";
        }
        else if(column == 5) {
            return "Max. Visual.";
        }
        return "";
    }
    
    public void reloadTable() {
        this.fireTableDataChanged();
    }
    
    public UserDocument getUserDocumentByIndex(int index) {
        return this.userDocumentsList.get(index);
    }
}