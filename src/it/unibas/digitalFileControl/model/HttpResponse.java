/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.model;

/**
 *
 * @author Gabriele D
 */
public class HttpResponse {
    public int StatusCode;
    public String Response;

    public HttpResponse(int StatusCode, String Response) {
        this.StatusCode = StatusCode;
        this.Response = Response;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public String getResponse() {
        return Response;
    }
}
