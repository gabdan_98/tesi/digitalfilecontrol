/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.model;

/**
 *
 * @author Gabriele D
 */
public class Enumerator {
    
    public class Storage {
        public static final String SHARED_WITH_ME_DOCUMENTS = "SHARED_WITH_ME_DOCUMENTS";
        public static final String MY_SHARED_DOCUMENTS = "MY_SHARED_DOCUMENTS";
        public static final String DETAILS_USERDOCUMENTS = "DETAILS_USERDOCUMENTS";
        public static final String DETAILS_UNAUTHORIZED_USERS = "DETAILS_UNAUTHORIZED_USERS";
        public static final String VIEW_SELECTED_DOCUMENT = "VIEW_SELECTED_DOCUMENT";
        public static final String DETAIL_SELECTED_DOCUMENT = "DETAIL_SELECTED_DOCUMENT";
        public static final String ADD_USERDOCUMENT_REQUEST = "ADD_USERDOCUMENT_REQUEST";
        public static final String ADD_USERDOCUMENT_USER = "ADD_USERDOCUMENT_USER";
        public static final String ADD_USERDOCUMENT_SELECTED_INDEX = "ADD_USERDOCUMENT_SELECTED_INDEX";
        public static final String UPLOADED_FILE_PATH = "UPLOADED_FILE_PATH";
        public static final String LOGGED_USER = "LOGGED_USER";
        public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
        public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    }
    
    public class Configuration {
        public static final String WEB_SERVICE_URL = "WEB_SERVICE_URL";
    }
    
    public class HttpRequestTypes {
        public static final String POST = "POST";
        public static final String GET = "GET";
        public static final String PUT = "PUT";
        public static final String DELETE = "DELETE";
    }
    
    public class FileTypes {
        public static final int PDF = 1;
    }
}