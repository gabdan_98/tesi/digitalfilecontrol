/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Gabriele D
 */
public class LocalStorage {
    private Map<String, Object> beans = new HashMap<>();

    public Object getBeans(String chiave) {
        return beans.get(chiave);
    }

    public void putBeans(String chiave, Object valore) {
        this.beans.put(chiave, valore);
    }
}