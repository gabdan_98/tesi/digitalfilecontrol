/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.model;

/**
 *
 * @author Gabriele D
 */
public class UserDocument {
    private int id;
    private String userId;
    private String username;
    private String name;
    private String surname;
    private String email;
    private boolean canView;
    private boolean canPrint;
    private boolean canDownload;
    private Integer viewsNumber;
    private Integer maxViewsNumber;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public Integer getMaxViewsNumber() {
        return maxViewsNumber;
    }

    public int getId() {
        return id;
    }

    public UserDocument(int id, String userId, String username, String name, String surname, String email, boolean canView, boolean canPrint, boolean canDownload, Integer viewsNumber, Integer maxViewsNumber) {
        this.id = id;
        this.userId = userId;
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.canView = canView;
        this.canPrint = canPrint;
        this.canDownload = canDownload;
        this.viewsNumber = viewsNumber;
        this.maxViewsNumber = maxViewsNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isCanView() {
        return canView;
    }

    public void setCanView(boolean canView) {
        this.canView = canView;
    }

    public boolean isCanPrint() {
        return canPrint;
    }

    public void setCanPrint(boolean canPrint) {
        this.canPrint = canPrint;
    }

    public boolean isCanDownload() {
        return canDownload;
    }

    public void setCanDownload(boolean canDownload) {
        this.canDownload = canDownload;
    }

    public Integer getViewsNumber() {
        return viewsNumber;
    }

    public void setViewsNumber(Integer viewsNumber) {
        this.viewsNumber = viewsNumber;
    }
    
    
}
