/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.exception;

/**
 *
 * @author Gabriele D
 */
public class EncryptionException extends Exception {
    
    public EncryptionException() {
        
    }
    
    public EncryptionException(String message) {
        super(message);
    }
}
