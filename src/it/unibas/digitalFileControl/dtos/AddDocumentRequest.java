/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.dtos;

/**
 *
 * @author Gabriele D
 */
public class AddDocumentRequest {
    private String name;
    private String description;
    public boolean documentExpire;
    private Long expirationTime;
    private int documentTypeId;
    private String content;

    public AddDocumentRequest(String name, String description, boolean documentExpire, Long expirationTime, int documentTypeId, String content) {
        this.name = name;
        this.description = description;
        this.documentExpire = documentExpire;
        this.expirationTime = expirationTime;
        this.documentTypeId = documentTypeId;
        this.content = content;
    }

    public boolean isDocumentExpire() {
        return documentExpire;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Long getExpirationTime() {
        return expirationTime;
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public String getContent() {
        return content;
    }
}
