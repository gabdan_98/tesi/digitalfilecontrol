/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.dtos;

import it.unibas.digitalFileControl.persistence.CustomDAOServiceResponse;
import it.unibas.digitalFileControl.model.Document;

/**
 *
 * @author Gabriele D
 */
public class GetDocumentResponse extends CustomDAOServiceResponse {
    public Document document;

    public GetDocumentResponse() {
    }

    public GetDocumentResponse(Document document) {
        this.document = document;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
