/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.dtos;

import it.unibas.digitalFileControl.persistence.CustomDAOServiceResponse;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.model.User;
import it.unibas.digitalFileControl.model.UserDocument;
import java.util.List;

/**
 *
 * @author Gabriele D
 */
public class GetDocumentDetailResponse extends CustomDAOServiceResponse {
    public Document document;
    public List<UserDocument> userDocuments;
    public List<User> unauthorizedUsers;

    public GetDocumentDetailResponse() {
    }

    public Document getDocument() {
        return document;
    }

    public List<User> getUnauthorizedUsers() {
        return unauthorizedUsers;
    }

    public List<UserDocument> getUserDocuments() {
        return userDocuments;
    }   
}