/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.dtos;

import it.unibas.digitalFileControl.persistence.CustomDAOServiceResponse;

/**
 *
 * @author Gabriele D
 */
public class DeleteUserFromDocumentResponse extends CustomDAOServiceResponse {

    public DeleteUserFromDocumentResponse() {
        super();
    }
}
