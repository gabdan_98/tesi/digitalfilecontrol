/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.dtos;

import it.unibas.digitalFileControl.persistence.CustomDAOServiceResponse;
import it.unibas.digitalFileControl.model.Document;
import java.util.List;

/**
 *
 * @author Gabriele D
 */
public class GetSharedWithMeDocumentsResponse extends CustomDAOServiceResponse {
    public List<Document> documents;

    public GetSharedWithMeDocumentsResponse(List<Document> documents) {
        this.documents = documents;
    }

    public GetSharedWithMeDocumentsResponse() {
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public List<Document> getDocuments() {
        return documents;
    }
}
