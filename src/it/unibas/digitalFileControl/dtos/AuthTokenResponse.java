/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.dtos;

import it.unibas.digitalFileControl.model.User;
import it.unibas.digitalFileControl.persistence.CustomDAOServiceResponse;

/**
 *
 * @author Gabriele D
 */
public class AuthTokenResponse extends CustomDAOServiceResponse {
    public String accessToken;
    public String refreshToken;
    public User user;
    
    public AuthTokenResponse() {
        
    }

    public AuthTokenResponse(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public User getUser() {
        return user;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
