/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.dtos;

/**
 *
 * @author Gabriele D
 */
public class AddUserDocumentRequest {
    public String userId;
    public int documentId;
    public boolean canView;
    public boolean canPrint;
    public boolean canDownload;
    public Integer maxViewsNumber;

    public AddUserDocumentRequest(String userId, int documentId, boolean canView, boolean canPrint, boolean canDownload, Integer maxViewsNumber) {
        this.userId = userId;
        this.documentId = documentId;
        this.canView = canView;
        this.canPrint = canPrint;
        this.canDownload = canDownload;
        this.maxViewsNumber = maxViewsNumber;
    }

    public AddUserDocumentRequest() {
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCanView(boolean canView) {
        this.canView = canView;
    }

    public String getUserId() {
        return userId;
    }

    public int getDocumentId() {
        return documentId;
    }

    public void setDocumentId(int documentId) {
        this.documentId = documentId;
    }

    public boolean isCanView() {
        return canView;
    }

    public boolean isCanPrint() {
        return canPrint;
    }

    public boolean isCanDownload() {
        return canDownload;
    }

    public Integer getMaxViewsNumber() {
        if(maxViewsNumber == null) {
            return null;
        } else {
            return maxViewsNumber;
        }
    }

    public void setCanPrint(boolean canPrint) {
        this.canPrint = canPrint;
    }

    public void setCanDownload(boolean canDownload) {
        this.canDownload = canDownload;
    }

    public void setMaxViewsNumber(Integer maxViewsNumber) {
        this.maxViewsNumber = maxViewsNumber;
    }
}