/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.test.persistence;

import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.dtos.AuthTokenResponse;
import it.unibas.digitalFileControl.persistence.DAOAccountService;
import it.unibas.digitalFileControl.utils.HttpClient;
import junit.framework.TestCase;

/**
 *
 * @author Gabriele D
 */
public class DAOAccountServiceTest extends TestCase {
    
    private DAOAccountService daoAccount = new DAOAccountService("https://digitalfilecontrolws.azurewebsites.net/api", new HttpClient());
    
    public DAOAccountServiceTest() {
        
    }
    
    public void testLoginSuccess() {
        try {
            AuthTokenResponse response = daoAccount.getAuthTokens("gabriele.dandrea", "Password_00");
            assertEquals(200, response.getStatusCode());
        }
        catch (Exception ex) {
            fail();
        }
    }
    
    public void testLoginFailed1() {
        try {
            AuthTokenResponse response = daoAccount.getAuthTokens("gabriele.dandreaa", "Password_00");
            assertEquals(412, response.getStatusCode());
        }
        catch (Exception ex) {
            fail();
        }
    }
    
    public void testLoginFailed2() {
        try {
            AuthTokenResponse response = daoAccount.getAuthTokens("gabriele.dandrea", "Password_01");
            assertEquals(412, response.getStatusCode());
        }
        catch (Exception ex) {
            fail();
        }
    }
    
    public void testPreventBruteForce() {
        try {
            AuthTokenResponse response;
            response = daoAccount.getAuthTokens("mario.rossi", "Password_01");
            response = daoAccount.getAuthTokens("mario.rossi", "Password_01");
            response = daoAccount.getAuthTokens("mario.rossi", "Password_01");
            response = daoAccount.getAuthTokens("mario.rossi", "Password_01");
            response = daoAccount.getAuthTokens("mario.rossi", "Password_01");
            response = daoAccount.getAuthTokens("mario.rossi", "Password_00");
            assertEquals(429, response.getStatusCode());
        } catch (Exception ex) {
            fail();
        }
    }
}
