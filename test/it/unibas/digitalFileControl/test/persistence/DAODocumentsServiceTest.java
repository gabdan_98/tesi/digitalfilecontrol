/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package it.unibas.digitalFileControl.test.persistence;

import it.unibas.digitalFileControl.dtos.AuthTokenResponse;
import it.unibas.digitalFileControl.dtos.GetDocumentResponse;
import it.unibas.digitalFileControl.model.Document;
import it.unibas.digitalFileControl.persistence.DAOAccountService;
import it.unibas.digitalFileControl.persistence.DAODocumentsService;
import it.unibas.digitalFileControl.utils.FileHelper;
import it.unibas.digitalFileControl.utils.HttpClient;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author Gabriele D
 */
public class DAODocumentsServiceTest extends TestCase {
    
    private DAODocumentsService daoDocument = new DAODocumentsService("https://digitalfilecontrolws.azurewebsites.net/api", new HttpClient());
    private DAOAccountService daoAccount = new DAOAccountService("https://digitalfilecontrolws.azurewebsites.net/api", new HttpClient());
    
    public DAODocumentsServiceTest() {
        
    }
    
    public void testGetDocument() {
        try {
            AuthTokenResponse response = daoAccount.getAuthTokens("gabriele.dandrea", "Password_00");
            daoDocument.getHttpClient().setAccessToken(response.getAccessToken());
            List<Document> userDocuments = new ArrayList<Document>();
            userDocuments.add(new Document(3, 3, "CUD", "CUD", 1, true, true, true, null, null, null, null));
            GetDocumentResponse result = daoDocument.getDocument(3, userDocuments);
            
            String encryptedContentPath = "mock/cud.txt";
            String encryptedContent = FileHelper.readTextFromFile(encryptedContentPath);
            
            assertEquals(encryptedContent, result.getDocument().getContent());
        } catch (Exception ex) {
            fail();
        }
    }
}
