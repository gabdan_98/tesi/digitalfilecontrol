/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package it.unibas.digitalFileControl.test.utils;

import it.unibas.digitalFileControl.Application;
import it.unibas.digitalFileControl.utils.EncryptionHelper;
import it.unibas.digitalFileControl.utils.FileHelper;
import junit.framework.TestCase;

/**
 *
 * @author Gabriele D
 */
public class EncryptionHelperTest extends TestCase {
    
    public EncryptionHelperTest() {
    }

    public void testDecryption1() {
        try {
            String encryptedContentPath = "mock/cud.txt";
            String encryptedContent = FileHelper.readTextFromFile(encryptedContentPath);
            
            String expectedContentPath = "testFolder/expectedCud.txt";
            String expectedContent = FileHelper.readTextFromFile(expectedContentPath);
            
            String decryptedContent = EncryptionHelper.decrypt(encryptedContent);
            assertEquals(expectedContent, decryptedContent);
        } catch (Exception ex) {
            fail();
        }
    }
    
    public void testDecryption2() {
        try {
            String encryptedContentPath = "mock/Delega.txt";
            String encryptedContent = FileHelper.readTextFromFile(encryptedContentPath);
            
            String expectedContentPath = "testFolder/expectedDelega.txt";
            String expectedContent = FileHelper.readTextFromFile(expectedContentPath);
            
            String decryptedContent = EncryptionHelper.decrypt(encryptedContent);
            assertEquals(expectedContent, decryptedContent);
        } catch (Exception ex) {
            fail();
        }
    }
    
    public void testDecryption3() {
        try {
            String encryptedContentPath = "mock/privacy.txt";
            String encryptedContent = FileHelper.readTextFromFile(encryptedContentPath);
            
            String expectedContentPath = "testFolder/expectedPrivacy.txt";
            String expectedContent = FileHelper.readTextFromFile(expectedContentPath);
            
            String decryptedContent = EncryptionHelper.decrypt(encryptedContent);
            assertEquals(expectedContent, decryptedContent);
        } catch (Exception ex) {
            fail();
        }
    }
}
